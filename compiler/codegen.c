/* MC900 - IMPLEMENTATION OF PROGRAMMING LANGUAGE
 * FINAL PROJECT: SL COMPILER
 * STUDENT: ELISA DELL'ARRIVA     RA: 135551
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"
#include "codegen.h"
#include "slc.h"

/*****************************/
/* GLOBAL VARIABLES          */
/*****************************/
int currentLevel = -1;
int displacement = 0;
int labelCounter = 0;
SymbolPointer symbolTable = NULL;
SymbolPointer currentFormals = NULL;
TypeDescriptorPointer typeDescriptorInteger, typeDescriptorBoolean;

/*****************************/
/* SYMBOL TABLE MANIPULATION */
/*****************************/
/* @function  createSymbol */
SymbolPointer createSymbol(SymbolCategory category, char *name) {
  SymbolPointer newSymbol;

  newSymbol = malloc(sizeof(Symbol));
  if (newSymbol == NULL) {
    printf("Memory alocation error\n");
    exit(-1);
  }

  newSymbol->category = category;
  newSymbol->identifier = name;
  newSymbol->next = NULL;
  switch (category) {
    case S_TYPE:
      newSymbol->descriptor.t = malloc(sizeof(struct type));
      break;
    case S_VARIABLE:
      newSymbol->descriptor.v = malloc(sizeof(struct variable));
      break;
    case S_LABEL:
      newSymbol->descriptor.l = malloc(sizeof(struct label));
      break;
    case S_CONSTANT:
      newSymbol->descriptor.c = malloc(sizeof(struct constant));
      break;
    case S_FUNCTION:
      newSymbol->descriptor.f = malloc(sizeof(struct function));
      break;
    case S_PARAMETER:
      newSymbol->descriptor.p = malloc(sizeof(struct param));
      break;
  }
  return newSymbol;
}

/* @function  insertSymbol */
void insertSymbol(SymbolPointer s) {
  SymbolPointer aux = NULL;

  aux = searchSymbol(s->identifier);
  if (aux == NULL || aux->level != s->level) {
    s->next = symbolTable;
    symbolTable = s;
  }
  else {
    SemanticError("Error: symbol duplication");
  }
}

/* @function  insertParam */
void insertParam(SymbolPointer s, SymbolPointer *formals) {
  SymbolPointer aux = NULL;

  aux = searchParam(s->identifier, *formals);
  if (aux == NULL || aux->level != s->level) {
    s->next = *formals;
    *formals = s;
  }
  else {
    SemanticError("Error: parameter duplication");
  }
}

/* @function  searchParam */
SymbolPointer searchParam(char *name, SymbolPointer formals) {
  SymbolPointer aux = formals;

  while (aux != NULL) {
    if (strcmp(aux->identifier,name) == 0) {
      return aux;
    }
    aux = aux->next;
  }
  return NULL;
}

/* @function  searchSymbol */
SymbolPointer searchSymbol(char *name) {
  SymbolPointer aux = symbolTable;

  while (aux != NULL) {
    if (strcmp(aux->identifier,name) == 0 && aux->level <= currentLevel) {
      return aux;
    }
    aux = aux->next;
  }
  return NULL;
}

/* @function  initSymbolTable */
void initSymbolTable() {
  SymbolPointer new;

  // primitive type: integer
  new = createSymbol(S_TYPE, "integer");
  new->level = -1;
  new->descriptor.t->type = malloc(sizeof(TypeDescriptor));
  new->descriptor.t->type->cat = TYPE_INTEGER;
  new->descriptor.t->type->size = 1;
  typeDescriptorInteger = new->descriptor.t->type;
  insertSymbol(new);

  // primitive type: boolean
  new = createSymbol(S_TYPE, "boolean");
  new->level = -1;
  new->descriptor.t->type = malloc(sizeof(TypeDescriptor));
  new->descriptor.t->type->cat = TYPE_BOOLEAN;
  new->descriptor.t->type->size = 1;
  typeDescriptorBoolean = new->descriptor.t->type;
  insertSymbol(new);

  // boolean constant: false
  new = createSymbol(S_CONSTANT, "false");
  new->level = -1;
  new->descriptor.c->value = 0;
  new->descriptor.c->type = malloc(sizeof(TypeDescriptor));
  new->descriptor.c->type->cat = TYPE_BOOLEAN;
  new->descriptor.c->type->size = 1;
  insertSymbol(new);

  // boolean constant: true
  new = createSymbol(S_CONSTANT, "true");
  new->level = -1;
  new->descriptor.c->value = 1;
  new->descriptor.c->type = malloc(sizeof(TypeDescriptor));
  new->descriptor.c->type->cat = TYPE_BOOLEAN;
  new->descriptor.c->type->size = 1;
  insertSymbol(new);

  // primitive function: write
  new = createSymbol(S_FUNCTION, "write");
  new->level = -1;
  new->descriptor.f->displacement = 0;
  new->descriptor.f->result = NULL;
  new->descriptor.f->params = NULL;
  insertSymbol(new);

  // primitive function: read
  new = createSymbol(S_FUNCTION, "read");
  new->level = -1;
  new->descriptor.f->displacement = 0;
  new->descriptor.f->result = NULL;
  new->descriptor.f->params = NULL;
  insertSymbol(new);
}

/************************/
/* PROGRAM PROCESSMENT  */
/************************/
int firstcall = 1;

/* Generates MEPA code for tree pointed by p */
void processProgram(void *p) {
  TreeNodePointer tree = (TreeNodePointer) p;

  initSymbolTable();
  if (firstcall) {
      processFunction(tree, 1);
      firstcall = 0;
  }
  else {
    processFunction(tree, 0);
  }
}

/* processFunction */
void processFunction(TreeNodePointer tree, int ismain) {
  char                   *fname = getIdentifier(tree->components[1]);
  TypeDescriptorPointer  returnType = NULL;
  int                    lastDispl = -4,
                         enterLabel,
                         functionsLabel,
                         returnLabel,
                         varsAloc = 0;
  SymbolPointer          formals = NULL,
                         oldFormals = NULL,
                         function,
                         aux;
  TreeNodePointer        block;

  currentLevel++;
  oldFormals = currentFormals;

  // generate new symbol and insert into symbol Table
  function = createSymbol(S_FUNCTION, fname);
  if (tree->components[0] != NULL) {
    returnType = getType(tree->components[0]);
  }
  if (tree->components[2] != NULL) {
    formals = processFormals(tree->components[2]->list, &lastDispl, 1);
  }
  if (returnType != NULL) {
    lastDispl = lastDispl - returnType->size;
  }
  function->descriptor.f->result = returnType;
  function->descriptor.f->params = formals;
  currentFormals = formals;
  function->descriptor.f->displacement = lastDispl;
  function->level = currentLevel - 1;
  insertSymbol(function);

  // generate MEPA code
  if (ismain) {
      generateMEPAcode(MAIN_I);
      function->descriptor.f->mepalabel = -1;
  }
  else {
    enterLabel = nextLabel();
    returnLabel = nextLabel();
    function->descriptor.f->mepalabel = enterLabel;
    generateMEPALabel(enterLabel);
    generateMEPAcode1(ENFN, currentLevel);
  }

  // process block
  if (tree->components[3] != NULL) {
    block = tree->components[3];
    for (int i=0; block->components[i] != NULL; i++) {
      switch (block->components[i]->category) {
        case COMPONENT_LABELS:
          processLabels(block->components[i]);
          break;
        case COMPONENT_VARS:
          varsAloc = processVars(block->components[i]->list);
          generateMEPAcode1(ALOC, varsAloc);
          break;
        case COMPONENT_TYPES:
          processTypes(block->components[i]->list);
          break;
        case COMPONENT_FUNCTIONS:
          displacement = displacement - varsAloc;
          functionsLabel = nextLabel();
          generateMEPAcodeLabel1(JUMP, functionsLabel);
          processFunctions(block->components[i]->list);
          generateMEPALabel(functionsLabel);
          displacement = displacement + varsAloc;
          break;
        case COMPONENT_BODY:
          processBody(block->components[i]->list, function->descriptor.f->displacement);
          break;
      }
    }
  }

  if (varsAloc != 0) {
    generateMEPAcode1(DLOC, varsAloc);
  }

  if (ismain) {
      generateMEPAcode(STOP);
      generateMEPAcode(END);
  }
  else {
    if (returnType != NULL) {
        generateMEPAcode1(RTRN, -lastDispl - 4 - returnType->size);
    }
    else{
      generateMEPAcode1(RTRN, -lastDispl - 4);
    }
  }
  currentFormals = oldFormals;
  currentLevel--;
}

SymbolPointer createParam(TreeNodePointer aux, TreeNodePointer params, int *lastDispl, int fullCount) {
  SymbolPointer   new;

  new = createSymbol(S_PARAMETER, getIdentifier(aux));
  new->level = currentLevel;
  new->descriptor.p->type = getType(params->components[1]);
  if (params->category == COMPONENT_VAL_PARAM) {
    new->descriptor.p->passage = PARAM_VALUE;
    if (fullCount) {
      new->descriptor.p->displacement = *lastDispl - new->descriptor.p->type->size;
      *lastDispl = *lastDispl - new->descriptor.p->type->size;
    }
    else {
      new->descriptor.p->displacement = *lastDispl - 1;
      *lastDispl = *lastDispl-1;
    }
  }
  else if (params->category == COMPONENT_REF_PARAM) {
    new->descriptor.p->passage = PARAM_VARIABLE;
    if (fullCount) {
      new->descriptor.p->displacement = *lastDispl - 1;
      *lastDispl = *lastDispl-1;
    }
    else {
      new->descriptor.p->displacement = *lastDispl;
    }
  }
  return new;
}

void processFormalsFunction(TreeNodePointer params, int *lastDispl, SymbolPointer *formalsTable) {
  SymbolPointer         function,
                        formals = NULL;
  TypeDescriptorPointer returnType = NULL;
  int                   formalsDisp = 0;

  if (params->components[0] != NULL) {
    returnType = getType(params->components[0]);
  }
  if (params->components[2] != NULL) {
    formals = processFormals(params->components[2]->list, lastDispl, 0);
  }
  if (returnType != NULL) {
    *lastDispl = *lastDispl - returnType->size;
  }
  function = createSymbol(S_FUNCTION, getIdentifier(params->components[1]));
  function->descriptor.f->result = returnType;
  function->descriptor.f->params = formals;
  function->descriptor.f->displacement = *lastDispl - 1;
  *lastDispl = *lastDispl - 1;
  function->level = currentLevel;
  insertParam(function, formalsTable);
}

SymbolPointer processFormals(TreeNodePointer params, int *lastDispl, int fullCount) {
  SymbolPointer   formals = NULL;
  TreeNodePointer aux;

  reverseList(&params);
  for (; params != NULL; params = params->list) {
    if (params->category == COMPONENT_FUNCTION_AS_PARAM) {
      processFormalsFunction(params, lastDispl, &formals);
    }
    else {
      if (params->components[0]->category == COMPONENT_IDENTIFIER) {
        aux = params->components[0];
      }
      else if (params->components[0]->category == COMPONENT_IDENTIFIER_LIST) {
        aux = params->components[0]->list;
        reverseList(&aux);
      }

      for (; aux != NULL; aux=aux->list) {
        insertParam(createParam(aux, params, lastDispl, fullCount), &formals);
      }
    }
  }
  return formals;
}

/* @function  processLabels */
void processLabels(TreeNodePointer tree) {
  SymbolPointer   newSymbol;
  char            *labelName;
  TreeNodePointer labels;

  if (tree->components[0]->category == COMPONENT_IDENTIFIER) {
    labels = tree->components[0];
  }
  else if (tree->components[0]->category == COMPONENT_IDENTIFIER_LIST) {
    labels = tree->components[0]->list;
  }
  for (; labels != NULL; labels=labels->list) {
    labelName = getIdentifier(labels);
    newSymbol = createSymbol(S_LABEL, labelName);
    newSymbol->level = currentLevel;
    newSymbol->descriptor.l->mepalabel = nextLabel();
    newSymbol->descriptor.l->defined = 0;
    insertSymbol(newSymbol);
  }
  return;
}

/* @function  processVars */
int processVars(TreeNodePointer variablesList) {
  SymbolPointer         newSymbol,
                        newSymbolType;
  char                  *varName,
                        *typeName;
  TreeNodePointer       aux;
  TypeDescriptorPointer varType = NULL;
  int                   aloc = 0;

  for (; variablesList != NULL; variablesList = variablesList->list) {
    // determine variable(s) type
    if (variablesList->components[1]->category == COMPONENT_VECTOR_TYPE) {
      varType = createTypeArray(variablesList->components[1]);
    }
    else if (variablesList->components[1]->category == COMPONENT_IDENTIFIER) {
      typeName = getIdentifier(variablesList->components[1]);
      newSymbolType = searchSymbol(typeName);
      if (newSymbolType == NULL) {
        SemanticError("Error: Type not yet declared.");
      }
      varType = newSymbolType->descriptor.t->type;
    }

    // insert new variable(s)
    if (variablesList->components[0]->category == COMPONENT_IDENTIFIER) {
      aux = variablesList->components[0];
    }
    else if (variablesList->components[0]->category == COMPONENT_IDENTIFIER_LIST) {
      aux = variablesList->components[0]->list;
    }
    for (; aux != NULL; aux=aux->list) {
      varName = getIdentifier(aux);
      newSymbol = createSymbol(S_VARIABLE, varName);
      newSymbol->level = currentLevel;
      newSymbol->descriptor.v->type = varType;
      newSymbol->descriptor.v->displacement = displacement;
      insertSymbol(newSymbol);
      displacement = displacement + newSymbol->descriptor.v->type->size;
      aloc = aloc + newSymbol->descriptor.v->type->size;
    }
  }
  return aloc;
}

/* @function  processTypes */
void processTypes(TreeNodePointer types) {
  char                  *typeName;
  SymbolPointer         new = NULL;
  TypeDescriptorPointer td = NULL;

  for (; types != NULL; types = types->list) {
    if (types->components[1]->category == COMPONENT_VECTOR_TYPE) {
      td = createTypeArray(types->components[1]);
    }

    typeName = getIdentifier(types->components[0]);
    new = createSymbol(S_TYPE, typeName);
    new->level = currentLevel;
    new->descriptor.t->type = td;
    insertSymbol(new);
  }
  return;
}

TypeDescriptorPointer createTypeArray(TreeNodePointer arrayType) {
  TreeNodePointer       aux;
  SymbolPointer         s_type = NULL;
  TypeDescriptorPointer type = NULL,
                        td = NULL;
  int                   totalSize = 1,
                        size,
                        dimension = 1;

  s_type = searchSymbol(getIdentifier(arrayType->components[0]));
  if (s_type == NULL) {
    SemanticError("Error: undefined type");
  }

  aux = arrayType->components[1]->list;
  reverseList(&aux);
  //create type descriptor for multidimension arrays
  for (; aux != NULL; aux = aux->list) {
    size = atoi(aux->string);
    dimension = dimension*(s_type->descriptor.t->type->size);
    totalSize *= s_type->descriptor.t->type->size * size;
    type = malloc(sizeof(TypeDescriptor));
    type->cat = TYPE_ARRAY;
    type->size = totalSize;
    type->descriptor.ta = malloc(sizeof(struct typeArray));
    type->descriptor.ta->element = s_type->descriptor.t->type;
    type->descriptor.ta->dimension = dimension;
    type->descriptor.ta->next = NULL;
    insertTypeArray(&td, type);
    dimension = size;
  }
  return td;
}

void insertTypeArray(TypeDescriptorPointer *td, TypeDescriptorPointer type) {
  type->descriptor.ta->next = *td;
  (*td) = type;
}

/* @function  processFunctions */
void processFunctions(TreeNodePointer functionsList) {

  for (; functionsList != NULL; functionsList = functionsList->list) {
    processFunction(functionsList, 0);
  }
}

/* @function  processBody */
void processBody(TreeNodePointer body, int lastDispl) {

  for (; body != NULL; body=body->list) {//keeps on processing statements
    switch (body->category) {
      case COMPONENT_FUNCALL:
        processFuncall(body);
        break;
      case COMPONENT_ASSIGN:
        processAssign(body);
        break;
      case COMPONENT_RETURN:
        processReturn(body, lastDispl);
        break;
      case COMPONENT_GOTO:
        processGoto(body->components[0]);
        break;
      case COMPONENT_LABEL_CALL:
        processLabelDef(body);
        break;
      case COMPONENT_IF:
        processConditional(body, lastDispl);
        break;
      case COMPONENT_WHILE:
        processRepetition(body, lastDispl);
        break;
    }
  }
}

/* @function  processReturn */
void processReturn(TreeNodePointer ret, int lastDispl) {
  if (ret != NULL) {
    if (processExpression(ret->components[0]) != NULL)
      generateMEPAcode2(STVL, currentLevel, lastDispl);
  }
}

/* @function  processRepetition */
void processRepetition(TreeNodePointer rep, int lastDispl) {
  int beginLabel = nextLabel(),
      endLabel = nextLabel();

  generateMEPALabel(beginLabel);
  processExpression(rep->components[0]);
  generateMEPAcodeLabel1(JMPF, endLabel);
  processBody(rep->components[1]->list, lastDispl);
  generateMEPAcodeLabel1(JUMP, beginLabel);
  generateMEPALabel(endLabel);
}

/* @function  processConditional */
void processConditional(TreeNodePointer cond, int lastDispl) {
  int elseLabel,
      endLabel;
  TypeDescriptorPointer resultType;

  //process the evaluation expression
  resultType = processExpression(cond->components[0]);
  if (resultType->cat != TYPE_BOOLEAN) {
    SemanticError("Error: conditional expression must be of type boolean");
  }

  endLabel = nextLabel();
  //IF block
  if (cond->components[2] != NULL) {
    elseLabel = nextLabel();
    generateMEPAcodeLabel1(JMPF, elseLabel);
  }
  else {
    generateMEPAcodeLabel1(JMPF, endLabel);
  }
  processBody(cond->components[1]->list, lastDispl);

  generateMEPAcodeLabel1(JUMP, endLabel);

  //ELSE block
  if (cond->components[2] != NULL) {
    generateMEPALabel(elseLabel);
    processBody(cond->components[2]->list, lastDispl);
    generateMEPAcodeLabel1(JUMP, endLabel);
  }

  generateMEPALabel(endLabel);
}

/* @function  processGoto */
void processGoto(TreeNodePointer node) {
  SymbolPointer s;

  s = searchSymbol(getIdentifier(node));
  if (s->category != S_LABEL) {
    SemanticError("Error: goto to something different of label");
  }
  generateMEPAcodeLabel1(JUMP, s->descriptor.l->mepalabel);
}

/* @function  processLabelDef */
void processLabelDef(TreeNodePointer node) {
  SymbolPointer s;

  s = searchSymbol(getIdentifier(node->components[0]));
  if (s->category != S_LABEL) {
    SemanticError("Error: not a label");
  }
  s->descriptor.l->defined = 1;
  generateMEPALabel(s->descriptor.l->mepalabel);
  generateMEPAcode2(ENLB, currentLevel, displacement);

  //operations
  if (node->components[1] != NULL) {
    processBody(node->components[1], -4);
  }
}

/* @function  processUnaryExpr */
TypeDescriptorPointer processUnaryExpr(TreeNodePointer exp) {
  TypeDescriptorPointer r1, r2;

  r1 = processExpression(exp->components[1]);
  r2 = processUnaryOperand(exp->components[0]);
  if (r1->cat != r2->cat) {
    SemanticError("Error: invalid type unary operator");
  }
  return r1;
}

/* @function  processExpression */
TypeDescriptorPointer processExpression(TreeNodePointer params) {
SymbolPointer         exprVar;
char                  *value;
TypeDescriptorPointer r;

  if (params == NULL) {
    return NULL;
  }

  switch (params->category) {
    case COMPONENT_INTEGER:
      value = getIdentifier(params);
      generateMEPAcode1(LDCT, atoi(value));
      return typeDescriptorInteger;
      break;
    case COMPONENT_FUNCALL:
      return processFuncall(params);
      break;
    case COMPONENT_BINARY_EXPRESSION:
      r = processBinaryExpr(params);
      return r;
      break;
    case COMPONENT_UNARY_EXPRESSION:
      return processUnaryExpr(params);
      break;
    case COMPONENT_IDENTIFIER: //variable
      return processIdentifier(params);
      break;
    case COMPONENT_VAR:
      return processArrayVar(params);
      break;
  }
}

TypeDescriptorPointer processArrayVar(TreeNodePointer array) {
  SymbolPointer         varAux;
  TypeDescriptorPointer type = NULL;
  int                   arrayLoadValue = 1,
                        displacement,
                        mepaArrayInstruction = LADR;
  char                  *name = getIdentifier(array->components[0]);

  varAux = searchParam(name, currentFormals);
  if (varAux == NULL) {
    varAux = searchSymbol(name);
    if (varAux == NULL) {
      SemanticError("Error: undefined variable/parameter");
    }
  }

  switch (varAux->category) {
    case S_VARIABLE:
      type = varAux->descriptor.v->type;
      displacement = varAux->descriptor.v->displacement;
      break;
    case S_PARAMETER:
      type = varAux->descriptor.p->type;
      displacement = varAux->descriptor.p->displacement;
      if (varAux->descriptor.p->passage == PARAM_VARIABLE) {
        mepaArrayInstruction = LDVL;
      }
      break;
  }

  generateMEPAcode2(mepaArrayInstruction, varAux->level, displacement);
  arrayLoadValue = loadArrayAdress(array, type);

  if (arrayLoadValue == 1) {
    generateMEPAcode(CONT);
  }
  else {
    generateMEPAcode1(LDMV, arrayLoadValue);
  }
  return type;
}

/* @function  processIdentifier */
TypeDescriptorPointer processIdentifier(TreeNodePointer params) {
  SymbolPointer     exprVar;
  char              *value = getIdentifier(params);

  exprVar = searchParam(value, currentFormals);
  if (exprVar == NULL) {
    exprVar = searchSymbol(value);
    if (exprVar == NULL) {
      SemanticError("Error: undeclared expression(variable or parameter)");
    }
  }
  switch (exprVar->category) {
    case S_FUNCTION:
      generateMEPAcodeLabel2(LGAD, exprVar->descriptor.f->mepalabel, currentLevel);
      break;
    case S_VARIABLE:
      if (exprVar->level > currentLevel) {
        SemanticError("Error: using undeclared variable");
      }

      if (exprVar->descriptor.v->type->cat == TYPE_ARRAY) {
          generateMEPAcode2(LADR, exprVar->level, exprVar->descriptor.v->displacement);
          generateMEPAcode1(LDMV, exprVar->descriptor.v->type->size);
      }
      else {
        generateMEPAcode2(LDVL, exprVar->level, exprVar->descriptor.v->displacement);
      }
      return exprVar->descriptor.v->type;
      break;
    case S_CONSTANT:
      generateMEPAcode1(LDCT, exprVar->descriptor.c->value);
      return exprVar->descriptor.c->type;
      break;
    case S_PARAMETER:
      if (exprVar->descriptor.p->type->cat == TYPE_ARRAY) {
          generateMEPAcode2(LADR, exprVar->level, exprVar->descriptor.p->displacement);
          generateMEPAcode1(LDMV, exprVar->descriptor.p->type->size);
      }
      else {
        if (exprVar->descriptor.p->passage == PARAM_VALUE) {
          generateMEPAcode2(LDVL, exprVar->level, exprVar->descriptor.p->displacement);
        }
        else if (exprVar->descriptor.p->passage == PARAM_VARIABLE) {
          generateMEPAcode2(LVLI, exprVar->level, exprVar->descriptor.p->displacement);
        }
      }
      return exprVar->descriptor.p->type;
      break;
  }
  return NULL;
}

/* @function  processUnaryOperand */
TypeDescriptorPointer processUnaryOperand(TreeNodePointer op) {
  if (strcmp(getIdentifier(op), "!") == 0) {
    generateMEPAcode(LNOT);
    return typeDescriptorBoolean;
  }
  else if (strcmp(getIdentifier(op), "-") == 0) {
    generateMEPAcode(NEGT);
    return typeDescriptorInteger;
  }
}

/* @function  processOperand */
TypeDescriptorPointer processOperand(TreeNodePointer op) {
  if (strcmp(getIdentifier(op), "+") == 0) {
    generateMEPAcode(ADDD);
    return typeDescriptorInteger;
  }
  else if (strcmp(getIdentifier(op), "-") == 0) {
    generateMEPAcode(SUBT);
    return typeDescriptorInteger;
  }
  else if (strcmp(getIdentifier(op), "*") == 0) {
    generateMEPAcode(MULT);
    return typeDescriptorInteger;
  }
  else if (strcmp(getIdentifier(op), "/") == 0) {
    generateMEPAcode(DIVI);
    return typeDescriptorInteger;
  }
  else if (strcmp(getIdentifier(op), "<") == 0) {
    generateMEPAcode(LESS);
    return typeDescriptorBoolean;
  }
  else if (strcmp(getIdentifier(op), "<=") == 0) {
    generateMEPAcode(LEQU);
    return typeDescriptorBoolean;
  }
  else if (strcmp(getIdentifier(op), ">") == 0) {
    generateMEPAcode(GRTR);
    return typeDescriptorBoolean;
  }
  else if (strcmp(getIdentifier(op), ">=") == 0) {
    generateMEPAcode(GEQU);
    return typeDescriptorBoolean;
  }
  else if (strcmp(getIdentifier(op), "&&") == 0) {
    generateMEPAcode(LAND);
    return typeDescriptorBoolean;
  }
  else if (strcmp(getIdentifier(op), "||") == 0) {
    generateMEPAcode(LORR);
    return typeDescriptorBoolean;
  }
  else if (strcmp(getIdentifier(op), "==") == 0) {
    generateMEPAcode(EQUA);
    return typeDescriptorBoolean;
  }
}

/* @function  processBinaryExpr */
TypeDescriptorPointer processBinaryExpr(TreeNodePointer exp) {
   TypeDescriptorPointer r1, r2;

   r1 = processExpression(exp->components[0]);
   r2 = processExpression(exp->components[2]);
   if ((r1 != NULL) && (r2 != NULL)) {
     if (r1->cat != r2->cat) {
       SemanticError("Error: expression with incompatible operands");
     }
   }
   return processOperand(exp->components[1]);
}

/* @function  processAssign */
void processAssign(TreeNodePointer body) {
  SymbolPointer         varAux;
  int                   mepaInstruction = STVL,
                        mepaArrayInstruction = LADR,
                        varDisplacement,
                        arrayStoreValue = -1;
  char                  *varName;
  TypeDescriptorPointer resultType,
                        varType;

  //gets variable name/identifier
  if (body->components[0]->category == COMPONENT_VAR) {
    varName = getIdentifier(body->components[0]->components[0]);
  }
  else {
    varName = getIdentifier(body->components[0]);
  }

  //checks if variable is defined
  varAux = searchParam(varName, currentFormals);
  if (varAux == NULL) {
    varAux = searchSymbol(varName);
    if (varAux == NULL || varAux->level > currentLevel) {
      SemanticError("Error: undeclared assign variable/parameter");
    }
  }

  switch (varAux->category) {
    case S_VARIABLE:
      varType = varAux->descriptor.v->type;
      varDisplacement = varAux->descriptor.v->displacement;
      break;
    case S_PARAMETER:
      varType = varAux->descriptor.p->type;
      varDisplacement = varAux->descriptor.p->displacement;
      if (varAux->descriptor.p->passage == PARAM_VARIABLE) {
        mepaInstruction = STVI;
        mepaArrayInstruction = LDVL;
      }
      break;
  }

  //in case of arrays, load correct indexes
  if (varType->cat == TYPE_ARRAY) {
    generateMEPAcode2(mepaArrayInstruction, varAux->level, varDisplacement);
    arrayStoreValue = loadArrayAdress(body->components[0], varType);
    varType = varType->descriptor.ta->element;
  }

  resultType = processExpression(body->components[1]);
  if (resultType->cat == TYPE_ARRAY || varType->cat == TYPE_ARRAY) {
  }
  else {
    if (resultType->cat != varType->cat) {
      SemanticError("Error: assigning imcompatible values");
    }
  }

  if (arrayStoreValue > -1) {
    generateMEPAcode1(STMV, arrayStoreValue);
  }
  else {
    generateMEPAcode2(mepaInstruction, varAux->level, varDisplacement);
  }
}

int loadArrayAdress(TreeNodePointer body, TypeDescriptorPointer type) {
  TypeDescriptorPointer index = type,
                        auxT;
  int                   arrayStoreValue = type->size;
  SymbolPointer         aux;


  if (body->category == COMPONENT_VAR) {
    if (body->components[1] != NULL && body->components[1]->category == COMPONENT_VAR_EXPR) {
      body = body->components[1]->list;
    }
    else if (body->components[1] == NULL){
      body = NULL;
    }
  }
  else if (body->category == COMPONENT_IDENTIFIER) {
    aux = searchParam(getIdentifier(body), currentFormals);
    if (aux == NULL) {
      aux = searchSymbol(getIdentifier(body));
      if (aux == NULL) {
        SemanticError("Error: undeclared variable/parameter");
      }
    }
    if (aux->category == S_VARIABLE) {
      auxT = aux->descriptor.v->type;
    }
    else if (aux->category == S_PARAMETER) {
      auxT = aux->descriptor.p->type;
    }
    if (auxT->cat == TYPE_ARRAY) {
      body = NULL;
    }
  }

  for (; body != NULL; body = body->list) {
    processExpression(body);
    generateMEPAcode1(INDX, index->descriptor.ta->dimension);
    arrayStoreValue = index->descriptor.ta->dimension;
    //process type element
    if (index->descriptor.ta->element->cat == TYPE_ARRAY) {
      arrayStoreValue = loadArrayAdress(body->list, index->descriptor.ta->element);
      body = body->list;
    }
    index = index->descriptor.ta->next;
  }
  return arrayStoreValue;
}

/* @function  processFuncall */
TypeDescriptorPointer processFuncall(TreeNodePointer funcall) {
  int                   isParam = 0;
  char                  *id;
  SymbolPointer         s,
                        funParams,
                        funDec;
  TreeNodePointer       params,
                        aux;
  TypeDescriptorPointer p1;

  id = getIdentifier(funcall->components[0]);

  if (strcmp(id, "write") == 0) {//function write
    if (funcall->components[1] != NULL) {//then, check parameter(s)
        if (funcall->components[1]->category == COMPONENT_EXPRESSION_LIST) {
          params = funcall->components[1]->list;
        }
        else {// there's only one parameter
          params = funcall->components[1];
        }

        for (; params != NULL; params=params->list) {
          processExpression(params);
          generateMEPAcode(PRNT);
        }
    }
    else {
      SemanticError("Error: missing arguments in function call");
    }
    return NULL;
  }
  else if (strcmp(id, "read") == 0) {//function read
    if (funcall->components[1] != NULL) {//then, check parameter(s)
      if (funcall->components[1]->category == COMPONENT_EXPRESSION_LIST) {
        params = funcall->components[1]->list;
      }
      else {// there's only one parameter
        params = funcall->components[1];
      }

      for (; params != NULL; params=params->list) {
        generateMEPAcode(READ);
        s = searchSymbol(getIdentifier(params));
        if (s == NULL) {
          SemanticError("Error: read undeclared variable");
        }
        generateMEPAcode2(STVL, currentLevel, s->descriptor.v->displacement);
      }
    }
    else {
      SemanticError("Error: missing arguments in function call");
    }
    return NULL;
  }
  else {//other functions
    funDec = searchSymbol(id);
    if (funDec == NULL) {
      funDec = searchParam(id, currentFormals);
      if (funDec == NULL) {
        SemanticError("Error: call to undeclared function");
      }
      isParam = 1;
    }

    // checar parâmentros estão de acordo
    if (funDec->descriptor.f->result != NULL) {
      generateMEPAcode1(ALOC, funDec->descriptor.f->result->size);
    }

    if (funDec->descriptor.f->params != NULL) {
      funParams = funDec->descriptor.f->params;
      if (funcall->components[1]->category == COMPONENT_EXPRESSION_LIST) {
        aux = funcall->components[1]->list;
      }
      else {
        aux = funcall->components[1];
      }
      for (; aux != NULL; aux=aux->list, funParams=funParams->next) {
        if (funParams->category != S_FUNCTION && funParams->descriptor.p->type->cat == TYPE_ARRAY && funParams->descriptor.p->passage == PARAM_VARIABLE) {
          p1 = processParamArrayByRef(aux);
        }
        else {
          p1 = processExpression(aux);
        }
      }
    }

    //generate MEPA code
    if (isParam) {
      generateMEPAcode3(CPFN, funDec->level, funDec->descriptor.f->displacement, currentLevel);
    }
    else {
      generateMEPAcodeLabel2(CFUN, funDec->descriptor.f->mepalabel, currentLevel);
    }
    return funDec->descriptor.f->result;
  }
}

TypeDescriptorPointer processParamArrayByRef(TreeNodePointer exprVar) {
  SymbolPointer a;
  char          *value;

  value = getIdentifier(exprVar);
  a = searchParam(value, currentFormals);
  if (a == NULL) {
    a = searchSymbol(value);
    if (a == NULL) {
      SemanticError("Error: undeclared expression(variable or parameter)");
    }
  }

  if (a->category == S_PARAMETER && a->descriptor.p->passage == PARAM_VARIABLE) {
       generateMEPAcode2(LDVL, a->level, a->descriptor.v->displacement);
  }
  else {
    generateMEPAcode2(LADR, a->level, a->descriptor.v->displacement);
  }
  return a->descriptor.v->type;
}

/************************/
/* MEPA CODE GENERATION */
/************************/
void generateMEPALabel(int label) {
  printf("L%d: NOOP\n", label);
}

void generateMEPAcode(int instruction) {
  switch (instruction) {
    case MAIN_I:
      printf("MAIN\n");
      break;
    case STOP:
      printf("STOP\n");
      break;
    case END:
      printf("END\n");
      break;
    case PRNT:
      printf("PRNT\n");
      break;
    case READ:
      printf("READ\n");
      break;
    case ADDD:
      printf("ADDD\n");
      break;
    case MULT:
      printf("MULT\n");
      break;
    case DIVI:
      printf("DIVI\n");
      break;
    case SUBT:
      printf("SUBT\n");
      break;
    case LESS:
      printf("LESS\n");
      break;
    case LEQU:
      printf("LEQU\n");
      break;
    case GRTR:
      printf("GRTR\n");
      break;
    case GEQU:
      printf("GEQU\n");
      break;
    case EQUA:
      printf("EQUA\n");
      break;
    case DIFF:
      printf("DIFF\n");
      break;
    case LNOT:
      printf("LNOT\n");
      break;
    case NEGT:
      printf("NEGT\n");
      break;
    case LAND:
      printf("LAND\n");
      break;
    case LORR:
      printf("LORR\n");
      break;
    case CONT:
      printf("CONT\n");
  }

  return;
}

void generateMEPAcode1(int instruction, int param) {
  switch (instruction) {
    case ENFN:
      printf("ENFN %d\n", param);
      break;
    case ALOC:
      printf("ALOC %d\n", param);
      break;
    case DLOC:
      printf("DLOC %d\n", param);
      break;
    case RTRN:
      printf("RTRN %d\n", param);
      break;
    case LDCT:
      printf("LDCT %d\n", param);
      break;
    case LDMV:
      printf("LDMV %d\n", param);
      break;
    case STMV:
      printf("STMV %d\n", param);
      break;
    case INDX:
      printf("INDX %d\n", param);
  }
}

void generateMEPAcode2(int instruction, int param1, int param2) {
  switch (instruction) {
    case LDVL:
      printf("LDVL %d,%d\n", param1, param2);
      break;
    case STVL:
      printf("STVL %d,%d\n", param1, param2);
      break;
    case ENLB:
      printf("ENLB %d,%d\n", param1, param2);
      break;
    case LADR:
      printf("LADR %d,%d\n", param1, param2);
      break;
    case LVLI:
      printf("LVLI %d,%d\n", param1, param2);
      break;
    case STVI:
      printf("STVI %d,%d\n", param1, param2);
      break;
  }
}

void generateMEPAcode3(int instruction, int param1, int param2, int param3) {
  switch (instruction) {
    case CPFN:
      printf("CPFN %d,%d,%d\n", param1, param2, param3);
      break;
  }
}

void generateMEPAcodeLabel1(int instruction, int param) {
  switch (instruction) {
    case JUMP:
      printf("JUMP L%d\n", param);
      break;
    case JMPF:
      printf("JMPF L%d\n", param);
      break;
  }
}

void generateMEPAcodeLabel2(int instruction, int param1, int param2) {
  switch (instruction) {
    case CFUN:
      printf("CFUN L%d,%d\n", param1, param2);
      break;
    case LGAD:
      printf("LGAD L%d,%d\n", param1, param2);
      break;
  }
}

/************************/
/* AUXILIARE FUNCTIONS  */
/************************/
char* getIdentifier(TreeNodePointer node) {
  char *id;

  if (*(node->string) == '+' || *(node->string) == '-' || *(node->string) == '*' || *(node->string) == '/') {
    *((node->string)+1) = '\0';
    return node->string;
  }
  else if (*(node->string) == '<' || *(node->string) == '>' || *(node->string) == '=' || *(node->string) == '&' || *(node->string) == '|' || *(node->string) == '!') {
    if (*(node->string + 1) == '=' || *(node->string + 1) == '&' || *(node->string + 1) == '|') {
      *((node->string)+2) = '\0';
    }
    else {
      *((node->string)+1) = '\0';
    }
    return node->string;
  }
  for (id = node->string; 1; id++) {
    if (*id == ' ' || *id == '(' || *id == ')' || *id == ';' || *id == ',' || *id == ':' || *id == '+' || *id == '-' || *id == '*') {
      *id = '\0';
      break;
    }
    else if (*id == '/' || *id == '<' || *id == '>' || *id == '!' || *id == '&' || *id == '|' || *id == '=' || *id == '[' || *id == ']') {
      *id = '\0';
      break;
    }
    else if (*id == '\0') {
      break;
    }
  }
  return node->string;
}

TypeDescriptorPointer getType(TreeNodePointer node) {
  char  *nodeName = getIdentifier(node);
  SymbolPointer aux;

  aux = searchSymbol(nodeName);
  if (aux != NULL && aux->category == S_TYPE) {
    return aux->descriptor.t->type;
  }
  return NULL;
}

int nextLabel() {
  labelCounter++;
  return labelCounter;
}

/* Reverse the list that is at the top of the stack */
void reverseList(TreeNodePointer *list) {
  TreeNodePointer l, aux, rl=NULL;

  l = *list;
  while (l != NULL) {
    aux = l->list;
    l->list = rl;
    rl = l;
    l = aux;
  }
  *list = rl;
}
