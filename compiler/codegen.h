/* MC900 - IMPLEMENTATION OF PROGRAMMING LANGUAGE
 * FINAL PROJECT: SL COMPILER
 * STUDENT: ELISA DELL'ARRIVA     RA: 135551
 */

/***********************/
/* PPROGRAM STRUCTURES */
/***********************/
/****************************/
/* Structures: SYMBOL TABLE */
/****************************/
typedef enum {
  S_CONSTANT = 1,
  S_VARIABLE,
  S_PARAMETER,
  S_FUNCTION,
  S_LABEL,
  S_TYPE
} SymbolCategory;

/*******************************/
/* Structures: TYPE DESCRIPTOR */
/*******************************/
typedef enum {
  TYPE_INTEGER = 1,
  TYPE_BOOLEAN,
  TYPE_ARRAY,
  TYPE_FUNCTION
} TypeCategory;

typedef struct typeDescriptor {
  TypeCategory  cat;
  int           size;   // how many spaces it needs at the stack
  union {
    struct typeArray {
      struct typeDescriptor      *element;
      struct typeDescriptor      *next;
      int                        dimension;
    } *ta;
    // struct typeFunction {
    //   struct typeDescriptor      *result;
    //   struct symbol              *params;
    // } *tf;
  } descriptor;
} TypeDescriptor, *TypeDescriptorPointer;

typedef enum {
  PARAM_VALUE = 1,
  PARAM_VARIABLE
} ParameterPassage;

typedef struct symbol {
  SymbolCategory  category;
  char            *identifier;
  int             level;
  struct symbol   *next;
  union {
    struct constant {
      TypeDescriptorPointer      type;
      int                        value;
    } *c;
    struct variable {
      int                        displacement;
      TypeDescriptorPointer      type;
    } *v;
    struct param {
      int                        displacement;
      TypeDescriptorPointer      type;
      ParameterPassage           passage;
    } *p;
    struct function {
      int                        displacement;
      TypeDescriptorPointer      result;
      struct symbol              *params;
      int                        mepalabel;
    } *f;
    struct label {
      int                        mepalabel;
      int                        defined;
    } *l;
    struct type{
      TypeDescriptorPointer      type;
    } *t;
  } descriptor;
} Symbol, *SymbolPointer;

/**********************************/
/* Definitions: MEPA instructions */
/**********************************/
#define MAIN_I 1
#define STOP 2
#define END  3
#define LDCT 4
#define LDVL 5
#define LADR 6
#define STVL 7
#define LVLI 8
#define STVI 9
#define ADDD 10
#define SUBT 11
#define MULT 12
#define DIVI 13
#define NEGT 14
#define LAND 15
#define LORR 16
#define LNOT 17
#define LESS 18
#define GRTR 19
#define EQUA 20
#define DIFF 22
#define LEQU 23
#define GEQU 24
#define JUMP 25
#define JMPF 26
#define NOOP 27
#define READ 28
#define PRNT 29
#define ALOC 30
#define DLOC 31
#define ENLB 32
#define LGAD 33
#define CFUN 34
#define CPFN 35
#define ENFN 36
#define RTRN 37
#define INDX 38
#define CONT 39
#define LDMV 40
#define STMV 41

/*************/
/* FUNCTIONS */
/*************/
/***************************************/
/* Functions: SymbolTable manipulation */
/***************************************/
void initSymbolTable();
void insertSymbol(SymbolPointer s);
void removeSymbol(SymbolPointer s);
void insertParam(SymbolPointer s, SymbolPointer *formals);
SymbolPointer searchSymbol(char *name);
SymbolPointer searchParam(char *name, SymbolPointer formals);
SymbolPointer createSymbol(SymbolCategory category, char *name);
SymbolPointer createParam(TreeNodePointer aux, TreeNodePointer params, int *lastDispl, int fullCount);

/**********************/
/* Functions: general */
/**********************/
void processProgram(void *p);
int stackHeight();

/*********************************/
/* Functions: process components */
/*********************************/
int processVars(TreeNodePointer variablesList);
void processGoto(TreeNodePointer node);
void processTypes(TreeNodePointer types);
void processLabels(TreeNodePointer tree);
void processAssign(TreeNodePointer body);
void processLabelDef(TreeNodePointer node);
void processFunction(TreeNode *tree, int ismain);
void processFunctions(TreeNodePointer functionsList);
void processBody(TreeNodePointer body, int lastDispl);
void processReturn(TreeNodePointer ret, int lastDispl);
void processRepetition(TreeNodePointer rep, int lastDispl);
void processConditional(TreeNodePointer cond, int lastDispl);
void processFormalsFunction(TreeNodePointer params, int *lastDispl, SymbolPointer *formalsTable);
SymbolPointer processFormals(TreeNodePointer params, int *lastDispl, int fullCount);
TypeDescriptorPointer processOperand(TreeNodePointer op);
TypeDescriptorPointer processUnaryExpr(TreeNodePointer exp);
TypeDescriptorPointer processBinaryExpr(TreeNodePointer exp);
TypeDescriptorPointer processFuncall(TreeNodePointer funcall);
TypeDescriptorPointer processUnaryOperand(TreeNodePointer op);
TypeDescriptorPointer processExpression(TreeNodePointer params);
TypeDescriptorPointer processIdentifier(TreeNodePointer params);
void insertTypeArray(TypeDescriptorPointer *td, TypeDescriptorPointer type);
TypeDescriptorPointer createTypeArray(TreeNodePointer arrayType);
int loadArrayAdress(TreeNodePointer body, TypeDescriptorPointer type);
TypeDescriptorPointer processArrayVar(TreeNodePointer array);
TypeDescriptorPointer processParamArrayByRef(TreeNodePointer exprVar);

/*********************************/
/* Functions: process auxiliares */
/*********************************/
int nextLabel();
void reverseList(TreeNodePointer *list);
char* getIdentifier(TreeNodePointer node);
TypeDescriptorPointer getType(TreeNodePointer node);
/*********************************/
/* Functions: generate MEPA code */
/*********************************/
void generateMEPALabel(int label);
void generateMEPAcode(int instruction);
void generateMEPAcode1(int instruction, int param);
void generateMEPAcodeLabel1(int instruction, int label);
void generateMEPAcode2(int instruction, int param1, int param2);
void generateMEPAcodeLabel2(int instruction, int param1, int param2);
void generateMEPAcode3(int instruction, int param1, int param2, int param3);
