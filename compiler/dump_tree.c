/* MC900 - IMPLEMENTATION OF PROGRAMMING LANGUAGE
 * FINAL PROJECT: SL COMPILER
 * STUDENT: ELISA DELL'ARRIVA     RA: 135551
 */

#include <stdio.h>
#include <stdlib.h>
#include "tree.h"
#include "codegen.h"

/* Print a given number of tabs */
void printTabs(int tabs) {
  for (int k=0; k<tabs; k++) {
    printf("\t");
  }
}

/* Print a list component with proper tabulation */
void dumpList(TreeNodePointer tree, int tabs) {
  if (tree != NULL) {
    TreeNodePointer aux;

    printTabs(tabs);
    printf("Categoria: %d\n", tree->category);

    for (int i=0; i<COMPONENTS_NUMBER; i++) {
      if (tree->components[i] != NULL) {
        printTabs(tabs);
        printf("Component %d: \n", i);
        aux = tree->components[i];
        dumpTree(aux, tabs+1);
      }
    }
  }
}

/* Print a list; auxiliar to dumpNode */
void printLinkedList(TreeNodePointer p, int tabs) {
  while (p != NULL) {
    dumpList(p, tabs);
    p = p->list;
  }
}

/* Print one node of the tree */
void dumpNode(TreeNode node, int tabs) {
  printTabs(tabs);
  printf("Categoria: %d\n", node.category);

  if (node.string != NULL) {
    printTabs(tabs);
    printf("Value: %s\n", getIdentifier(&node));
  }

  if (node.list != NULL) {
    printTabs(tabs);
    printf("List: \n");
    printLinkedList(node.list, tabs+1);
  }
}

/* Print the Tree with proper tabulation */
void dumpTree(TreeNodePointer tree, int tabs) {
  if (tree != NULL) {
    TreeNodePointer aux;

    dumpNode(*tree, tabs);
    for (int i=0; i<COMPONENTS_NUMBER; i++) {
      if (tree->components[i] != NULL) {
        printTabs(tabs);
        printf("Component %d: \n", i);
        aux = tree->components[i];
        dumpTree(aux, tabs+1);
      }
    }
  }
  // else {
  //   printf("NULL\n");
  // }
}

/* Print the stack */
// void printStack() {
//   for (int aux = top; aux>0; aux--) {
//     dumpNode(*(stack[aux-1]), 0);
//   }
// }
