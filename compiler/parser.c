/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "parser.y" /* yacc.c:339  */

/* MC900 - IMPLEMENTATION OF PROGRAMMING LANGUAGE
 * FINAL PROJECT: SL COMPILER
 * STUDENT: ELISA DELL'ARRIVA     RA: 135551
 */

/* Declarations */
#include <stdio.h>
#include "parser.h"
#include "slc.h"
#include "tree.h"

int block_components[MAX_BLOCK_COMPONENTS];
int block_i=0;


#line 83 "parser.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parser.h".  */
#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    EQUAL = 258,
    DIFFERENT = 259,
    LESS = 260,
    LESS_OR_EQUAL = 261,
    GREATER = 262,
    GREATER_OR_EQUAL = 263,
    PLUS = 264,
    MINUS = 265,
    OR = 266,
    MULTIPLY = 267,
    DIV = 268,
    AND = 269,
    UNARY = 270,
    CLOSE_BRACE = 271,
    CLOSE_PAREN = 272,
    CLOSE_BRACKET = 273,
    COLON = 274,
    COMMA = 275,
    ELSE = 276,
    END_OF_FILE = 277,
    FUNCTIONS = 278,
    GOTO = 279,
    IDENTIFIER = 280,
    ASSIGN = 281,
    IF = 282,
    INTEGER = 283,
    LABELS = 284,
    NOT = 285,
    OPEN_BRACE = 286,
    OPEN_BRACKET = 287,
    OPEN_PAREN = 288,
    RETURN = 289,
    SEMI_COLON = 290,
    TYPES = 291,
    VAR = 292,
    VARS = 293,
    VOID = 294,
    WHILE = 295,
    UNFINISHED_COMMENT = 296,
    LEXICAL_ERROR = 297,
    MULT = 298
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 178 "parser.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  7
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   226

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  44
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  61
/* YYNRULES -- Number of rules.  */
#define YYNRULES  121
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  207

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   298

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    71,    71,    79,    79,    80,    83,    92,    92,    93,
      93,    94,    94,    95,    95,    96,    96,   101,   101,   105,
     109,   111,   115,   116,   120,   122,   123,   125,   130,   134,
     136,   141,   141,   145,   146,   150,   151,   154,   155,   163,
     165,   166,   170,   171,   174,   175,   178,   180,   182,   185,
     187,   187,   197,   198,   199,   200,   204,   205,   206,   207,
     208,   209,   210,   214,   215,   219,   220,   225,   230,   233,
     235,   240,   245,   247,   252,   254,   259,   264,   272,   273,
     278,   279,   283,   284,   288,   289,   290,   291,   291,   295,
     296,   300,   304,   305,   306,   307,   308,   308,   312,   313,
     317,   318,   322,   326,   327,   331,   332,   340,   341,   342,
     343,   344,   345,   349,   350,   354,   355,   356,   360,   361,
     362,   366
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "EQUAL", "DIFFERENT", "LESS",
  "LESS_OR_EQUAL", "GREATER", "GREATER_OR_EQUAL", "PLUS", "MINUS", "OR",
  "MULTIPLY", "DIV", "AND", "UNARY", "CLOSE_BRACE", "CLOSE_PAREN",
  "CLOSE_BRACKET", "COLON", "COMMA", "ELSE", "END_OF_FILE", "FUNCTIONS",
  "GOTO", "IDENTIFIER", "ASSIGN", "IF", "INTEGER", "LABELS", "NOT",
  "OPEN_BRACE", "OPEN_BRACKET", "OPEN_PAREN", "RETURN", "SEMI_COLON",
  "TYPES", "VAR", "VARS", "VOID", "WHILE", "UNFINISHED_COMMENT",
  "LEXICAL_ERROR", "MULT", "$accept", "program", "function", "$@1",
  "function_continuation", "block", "$@2", "$@3", "$@4", "$@5", "$@6",
  "labels", "$@7", "types", "type_declarations", "type", "array_type",
  "variables", "vars_declaration", "functions", "$@8",
  "functions_declaration", "body", "body_statements", "formal_parameters",
  "multiple_parameters", "formal_parameter", "expression_parameter",
  "function_parameter", "$@9", "statement", "unlabeled_statement",
  "compound", "compound_unlabeled_statements", "assignment",
  "function_call_statement", "function_call", "goto", "return",
  "conditional", "repetitive", "empty_statement", "expression",
  "expression_list", "multiple_expressions", "simple_expression", "$@10",
  "term", "term_declaration", "factor", "$@11", "variable",
  "variable_expression", "identifier", "identifier_list",
  "multiple_identifier", "relational_operator", "unary",
  "additive_operator", "multiplicative_operator", "integer", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298
};
# endif

#define YYPACT_NINF -135

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-135)))

#define YYTABLE_NINF -104

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      -6,  -135,  -135,    21,     5,    25,    25,  -135,  -135,  -135,
      26,  -135,    37,   158,  -135,    25,  -135,     1,  -135,  -135,
     119,    42,  -135,    25,    89,    25,    25,  -135,  -135,  -135,
    -135,  -135,  -135,    50,    54,    25,  -135,   110,    23,    25,
      26,    69,    25,    -6,  -135,  -135,    25,    53,   118,    74,
    -135,    61,   106,  -135,  -135,  -135,  -135,  -135,    60,  -135,
    -135,  -135,  -135,  -135,    77,    48,    25,    84,    25,    96,
     158,   158,   158,   158,  -135,    25,    26,  -135,  -135,   110,
    -135,  -135,    25,  -135,  -135,    -6,    92,    97,   146,  -135,
    -135,   135,    -3,  -135,  -135,  -135,  -135,   146,  -135,  -135,
     101,   196,   201,   202,  -135,   165,  -135,   146,  -135,  -135,
    -135,   146,    66,   146,   155,   116,    99,    25,   109,    25,
    -135,  -135,  -135,  -135,  -135,  -135,  -135,  -135,  -135,  -135,
    -135,   133,  -135,  -135,   165,   137,  -135,  -135,  -135,  -135,
    -135,  -135,  -135,   146,  -135,  -135,  -135,   146,  -135,  -135,
    -135,  -135,   165,   208,   140,   126,  -135,  -135,   149,  -135,
     148,   156,   146,    25,   142,   150,    25,   143,   153,  -135,
    -135,  -135,  -135,  -135,   201,   153,  -135,  -135,   146,   166,
    -135,   173,   157,  -135,    90,   181,   162,  -135,   184,   146,
    -135,  -135,   146,  -135,  -135,  -135,   203,    93,  -135,   153,
    -135,  -135,  -135,  -135,   204,  -135,  -135
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,   102,     3,     0,     0,     0,     0,     1,     2,     5,
       0,     4,     0,     0,    39,     0,    50,     0,    44,    45,
      48,     0,    31,     0,     0,     0,     0,     6,     7,     9,
      11,    13,    15,   103,     0,     0,    40,     0,     0,     0,
       0,   104,     0,     0,    17,    35,     0,     0,     0,     0,
      77,     0,     0,    37,    54,    55,    56,    57,     0,    58,
      59,    60,    61,    62,     0,    98,    19,     0,    28,     0,
       0,     0,     0,     0,    16,     0,     0,    42,    41,     0,
     105,    49,     0,    47,    33,    32,     0,     0,     0,    63,
      65,     0,    98,   114,   113,   121,    96,     0,    72,    94,
       0,    78,    84,    89,    92,     0,    93,     0,    36,    38,
      68,     0,     0,     0,     0,    99,     0,     0,     0,     0,
       8,    10,    12,    14,    46,    51,    43,   106,    34,    18,
      71,     0,    64,    66,     0,     0,    73,   107,   108,   109,
     110,   111,   112,     0,   115,   116,   117,     0,   118,   119,
     120,    90,     0,    86,     0,     0,    52,    53,     0,    69,
      80,     0,     0,     0,     0,    22,     0,     0,     0,    97,
      95,    79,    85,    91,     0,     0,    67,   100,     0,    81,
      70,     0,     0,    20,     0,    23,     0,    29,    74,     0,
      76,    82,     0,   101,    21,    24,     0,     0,    30,     0,
      88,    83,    25,    26,     0,    75,    27
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -135,  -135,   -41,  -135,   214,   136,  -135,  -135,  -135,  -135,
    -135,  -135,  -135,  -135,  -135,  -103,  -135,  -135,  -135,  -135,
    -135,  -135,  -135,  -135,   -23,  -135,   -30,  -135,  -135,  -135,
     171,   -40,  -111,  -135,  -135,  -135,   -20,  -135,  -135,  -135,
    -135,  -135,   -66,  -135,  -135,  -134,  -135,   120,  -135,   -65,
    -135,   -14,  -135,     0,   -12,  -135,  -135,  -135,    52,  -135,
    -119
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     3,     4,     6,     9,    27,    70,    71,    72,    73,
      74,    28,    86,    29,    66,   164,   185,    30,    68,    31,
      43,    85,    32,    52,    13,    38,    17,    18,    19,    35,
      53,    54,    55,    91,    56,    57,    99,    59,    60,    61,
      62,    63,   100,   161,   179,   101,   174,   102,   151,   103,
     134,   104,   115,    92,    21,    41,   143,   105,   147,   152,
     106
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
       5,   157,    84,    34,    58,    10,    10,    77,    90,   171,
      64,    44,    20,   172,    69,    33,   167,    81,    36,     1,
      40,     7,   131,    33,    65,    67,    33,     8,    58,   113,
     114,   135,    58,     2,    64,    76,    37,    20,    64,    80,
      78,   154,    83,     5,   128,   155,    87,   158,   160,   126,
       1,   133,    65,   125,    14,   200,   118,   188,    79,    12,
     182,    42,     1,   186,   190,   196,   116,   112,    33,   169,
      39,    58,   156,    75,    15,   124,    16,    64,   204,    20,
     113,   114,   127,    93,    94,     5,    88,   173,   205,    82,
      46,     1,    58,    47,   107,   110,   181,    48,    64,     1,
      49,    50,    95,   111,    96,    45,    51,    97,   195,    98,
     117,   203,   191,    46,     1,   119,    47,   165,    95,   165,
      48,    95,   108,    49,    50,   163,   201,   129,   166,    51,
      46,     1,   130,    47,    89,     1,   136,    48,  -103,    39,
      49,    50,    46,     1,     1,    47,    51,    15,   162,    16,
     168,   132,    49,    50,   170,    93,    94,   175,    51,    46,
       1,   176,    47,   165,    93,    94,   165,   177,   178,    49,
      50,     1,   159,   180,    95,    51,    96,   183,   187,    97,
       1,    22,   184,    95,    48,    96,   192,    23,    97,    24,
       1,   193,   194,    95,    25,    96,    26,   198,    97,   137,
     138,   139,   140,   141,   142,   199,   120,   121,   122,   123,
     144,   145,   146,   197,   148,   149,   150,   -87,   -87,   -87,
      11,   202,   206,   109,     0,   153,   189
};

static const yytype_int16 yycheck[] =
{
       0,   112,    43,    15,    24,     5,     6,    37,    48,   143,
      24,    23,    12,   147,    26,    15,   119,    40,    17,    25,
      20,     0,    88,    23,    24,    25,    26,    22,    48,    32,
      33,    97,    52,    39,    48,    35,    35,    37,    52,    39,
      17,   107,    42,    43,    85,   111,    46,   113,   114,    79,
      25,    91,    52,    76,    17,   189,    68,   168,    35,    33,
     163,    19,    25,   166,   175,   184,    66,    19,    68,   134,
      20,    91,   112,    19,    37,    75,    39,    91,   197,    79,
      32,    33,    82,     9,    10,    85,    33,   152,   199,    20,
      24,    25,   112,    27,    33,    35,   162,    31,   112,    25,
      34,    35,    28,    26,    30,    16,    40,    33,    18,    35,
      26,    18,   178,    24,    25,    19,    27,   117,    28,   119,
      31,    28,    16,    34,    35,    26,   192,    35,    19,    40,
      24,    25,    35,    27,    16,    25,    35,    31,    19,    20,
      34,    35,    24,    25,    25,    27,    40,    37,    32,    39,
      17,    16,    34,    35,    17,     9,    10,    17,    40,    24,
      25,    35,    27,   163,     9,    10,   166,    18,    20,    34,
      35,    25,    17,    17,    28,    40,    30,    35,    35,    33,
      25,    23,    32,    28,    31,    30,    20,    29,    33,    31,
      25,    18,    35,    28,    36,    30,    38,    35,    33,     3,
       4,     5,     6,     7,     8,    21,    70,    71,    72,    73,
       9,    10,    11,    32,    12,    13,    14,     9,    10,    11,
       6,    18,    18,    52,    -1,   105,   174
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    25,    39,    45,    46,    97,    47,     0,    22,    48,
      97,    48,    33,    68,    17,    37,    39,    70,    71,    72,
      97,    98,    23,    29,    31,    36,    38,    49,    55,    57,
      61,    63,    66,    97,    98,    73,    17,    35,    69,    20,
      97,    99,    19,    64,    98,    16,    24,    27,    31,    34,
      35,    40,    67,    74,    75,    76,    78,    79,    80,    81,
      82,    83,    84,    85,    95,    97,    58,    97,    62,    98,
      50,    51,    52,    53,    54,    19,    97,    70,    17,    35,
      97,    68,    20,    97,    46,    65,    56,    97,    33,    16,
      75,    77,    97,     9,    10,    28,    30,    33,    35,    80,
      86,    89,    91,    93,    95,   101,   104,    33,    16,    74,
      35,    26,    19,    32,    33,    96,    97,    26,    98,    19,
      49,    49,    49,    49,    97,    68,    70,    97,    46,    35,
      35,    86,    16,    75,    94,    86,    35,     3,     4,     5,
       6,     7,     8,   100,     9,    10,    11,   102,    12,    13,
      14,    92,   103,    91,    86,    86,    75,    76,    86,    17,
      86,    87,    32,    26,    59,    97,    19,    59,    17,    93,
      17,    89,    89,    93,    90,    17,    35,    18,    20,    88,
      17,    86,    59,    35,    32,    60,    59,    35,    76,   102,
      76,    86,    20,    18,    35,    18,   104,    32,    35,    21,
      89,    86,    18,    18,   104,    76,    18
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    44,    45,    47,    46,    46,    48,    50,    49,    51,
      49,    52,    49,    53,    49,    54,    49,    56,    55,    57,
      58,    58,    59,    59,    60,    60,    60,    60,    61,    62,
      62,    64,    63,    65,    65,    66,    66,    67,    67,    68,
      68,    68,    69,    69,    70,    70,    71,    71,    71,    72,
      73,    72,    74,    74,    74,    74,    75,    75,    75,    75,
      75,    75,    75,    76,    76,    77,    77,    78,    79,    80,
      80,    81,    82,    82,    83,    83,    84,    85,    86,    86,
      87,    87,    88,    88,    89,    89,    89,    90,    89,    91,
      91,    92,    93,    93,    93,    93,    94,    93,    95,    95,
      96,    96,    97,    98,    98,    99,    99,   100,   100,   100,
     100,   100,   100,   101,   101,   102,   102,   102,   103,   103,
     103,   104
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     0,     3,     2,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     2,     0,     4,     2,
       4,     5,     1,     2,     2,     3,     3,     4,     2,     4,
       5,     0,     3,     1,     2,     2,     3,     1,     2,     2,
       3,     4,     2,     3,     1,     1,     4,     3,     1,     3,
       0,     4,     3,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     2,     3,     1,     2,     4,     2,     3,
       4,     3,     2,     3,     5,     7,     5,     1,     1,     3,
       1,     2,     2,     3,     1,     3,     2,     0,     5,     1,
       2,     2,     1,     1,     1,     3,     0,     3,     1,     2,
       3,     4,     1,     1,     2,     2,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 71 "parser.y" /* yacc.c:1646  */
    {return 0;}
#line 1423 "parser.c" /* yacc.c:1646  */
    break;

  case 3:
#line 79 "parser.y" /* yacc.c:1646  */
    {generateEmpty();}
#line 1429 "parser.c" /* yacc.c:1646  */
    break;

  case 6:
#line 84 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_FUNCTION, 4); block_components[block_i]=0;}
#line 1435 "parser.c" /* yacc.c:1646  */
    break;

  case 7:
#line 92 "parser.y" /* yacc.c:1646  */
    {block_components[block_i]++;}
#line 1441 "parser.c" /* yacc.c:1646  */
    break;

  case 9:
#line 93 "parser.y" /* yacc.c:1646  */
    {block_components[block_i]++;}
#line 1447 "parser.c" /* yacc.c:1646  */
    break;

  case 11:
#line 94 "parser.y" /* yacc.c:1646  */
    {block_components[block_i]++;}
#line 1453 "parser.c" /* yacc.c:1646  */
    break;

  case 13:
#line 95 "parser.y" /* yacc.c:1646  */
    {block_components[block_i]++;}
#line 1459 "parser.c" /* yacc.c:1646  */
    break;

  case 15:
#line 96 "parser.y" /* yacc.c:1646  */
    {block_components[block_i]++;}
#line 1465 "parser.c" /* yacc.c:1646  */
    break;

  case 16:
#line 97 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_BLOCK, block_components[block_i]); block_components[block_i]=0;}
#line 1471 "parser.c" /* yacc.c:1646  */
    break;

  case 17:
#line 101 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_LABELS, 1);}
#line 1477 "parser.c" /* yacc.c:1646  */
    break;

  case 19:
#line 106 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_TYPES);}
#line 1483 "parser.c" /* yacc.c:1646  */
    break;

  case 20:
#line 110 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_TYPE_DECLARATION, 2);}
#line 1489 "parser.c" /* yacc.c:1646  */
    break;

  case 21:
#line 112 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_TYPE_DECLARATION, 2); insertTopList();}
#line 1495 "parser.c" /* yacc.c:1646  */
    break;

  case 23:
#line 117 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_VECTOR_TYPE_LIST); generateNode(COMPONENT_VECTOR_TYPE, 2);}
#line 1501 "parser.c" /* yacc.c:1646  */
    break;

  case 24:
#line 121 "parser.y" /* yacc.c:1646  */
    {generateCompleteNode(COMPONENT_EMPTY, 0, NULL);}
#line 1507 "parser.c" /* yacc.c:1646  */
    break;

  case 26:
#line 124 "parser.y" /* yacc.c:1646  */
    {generateCompleteNode(COMPONENT_EMPTY, 0, NULL); insertTopList();}
#line 1513 "parser.c" /* yacc.c:1646  */
    break;

  case 27:
#line 126 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1519 "parser.c" /* yacc.c:1646  */
    break;

  case 28:
#line 131 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_VARS);}
#line 1525 "parser.c" /* yacc.c:1646  */
    break;

  case 29:
#line 135 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_VAR_DECLARATION, 2);}
#line 1531 "parser.c" /* yacc.c:1646  */
    break;

  case 30:
#line 137 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_VAR_DECLARATION, 2); insertTopList();}
#line 1537 "parser.c" /* yacc.c:1646  */
    break;

  case 31:
#line 141 "parser.y" /* yacc.c:1646  */
    {block_i++;}
#line 1543 "parser.c" /* yacc.c:1646  */
    break;

  case 32:
#line 142 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_FUNCTIONS); block_i--;}
#line 1549 "parser.c" /* yacc.c:1646  */
    break;

  case 34:
#line 146 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1555 "parser.c" /* yacc.c:1646  */
    break;

  case 35:
#line 150 "parser.y" /* yacc.c:1646  */
    {generateEmpty();}
#line 1561 "parser.c" /* yacc.c:1646  */
    break;

  case 36:
#line 151 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_BODY);}
#line 1567 "parser.c" /* yacc.c:1646  */
    break;

  case 38:
#line 155 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1573 "parser.c" /* yacc.c:1646  */
    break;

  case 39:
#line 164 "parser.y" /* yacc.c:1646  */
    {generateEmpty();}
#line 1579 "parser.c" /* yacc.c:1646  */
    break;

  case 40:
#line 165 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_PARAMETERS);}
#line 1585 "parser.c" /* yacc.c:1646  */
    break;

  case 41:
#line 167 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_PARAMETERS);}
#line 1591 "parser.c" /* yacc.c:1646  */
    break;

  case 42:
#line 170 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1597 "parser.c" /* yacc.c:1646  */
    break;

  case 43:
#line 171 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1603 "parser.c" /* yacc.c:1646  */
    break;

  case 46:
#line 179 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_REF_PARAM, 2);}
#line 1609 "parser.c" /* yacc.c:1646  */
    break;

  case 47:
#line 181 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_VAL_PARAM, 2);}
#line 1615 "parser.c" /* yacc.c:1646  */
    break;

  case 49:
#line 186 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_FUNCTION_AS_PARAM, 3);}
#line 1621 "parser.c" /* yacc.c:1646  */
    break;

  case 50:
#line 187 "parser.y" /* yacc.c:1646  */
    {generateEmpty();}
#line 1627 "parser.c" /* yacc.c:1646  */
    break;

  case 51:
#line 188 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_FUNCTION_AS_PARAM, 3);}
#line 1633 "parser.c" /* yacc.c:1646  */
    break;

  case 52:
#line 197 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_LABEL_CALL, 2);}
#line 1639 "parser.c" /* yacc.c:1646  */
    break;

  case 53:
#line 198 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_LABEL_CALL, 2);}
#line 1645 "parser.c" /* yacc.c:1646  */
    break;

  case 63:
#line 214 "parser.y" /* yacc.c:1646  */
    {generateEmpty();}
#line 1651 "parser.c" /* yacc.c:1646  */
    break;

  case 64:
#line 216 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_COMPOUND);}
#line 1657 "parser.c" /* yacc.c:1646  */
    break;

  case 66:
#line 221 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1663 "parser.c" /* yacc.c:1646  */
    break;

  case 67:
#line 226 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_ASSIGN, 2);}
#line 1669 "parser.c" /* yacc.c:1646  */
    break;

  case 69:
#line 234 "parser.y" /* yacc.c:1646  */
    {generateEmpty(); generateNode(COMPONENT_FUNCALL, 2);}
#line 1675 "parser.c" /* yacc.c:1646  */
    break;

  case 70:
#line 236 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_FUNCALL, 2);}
#line 1681 "parser.c" /* yacc.c:1646  */
    break;

  case 71:
#line 241 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_GOTO, 1);}
#line 1687 "parser.c" /* yacc.c:1646  */
    break;

  case 72:
#line 246 "parser.y" /* yacc.c:1646  */
    {generateEmpty(); generateNode(COMPONENT_RETURN, 1);}
#line 1693 "parser.c" /* yacc.c:1646  */
    break;

  case 73:
#line 248 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_RETURN, 1);}
#line 1699 "parser.c" /* yacc.c:1646  */
    break;

  case 74:
#line 253 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_IF, 2);}
#line 1705 "parser.c" /* yacc.c:1646  */
    break;

  case 75:
#line 255 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_IF, 3);}
#line 1711 "parser.c" /* yacc.c:1646  */
    break;

  case 76:
#line 260 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_WHILE, 2);}
#line 1717 "parser.c" /* yacc.c:1646  */
    break;

  case 77:
#line 264 "parser.y" /* yacc.c:1646  */
    {generateEmpty();}
#line 1723 "parser.c" /* yacc.c:1646  */
    break;

  case 79:
#line 274 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_BINARY_EXPRESSION, 3);}
#line 1729 "parser.c" /* yacc.c:1646  */
    break;

  case 81:
#line 280 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_EXPRESSION_LIST);}
#line 1735 "parser.c" /* yacc.c:1646  */
    break;

  case 82:
#line 283 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1741 "parser.c" /* yacc.c:1646  */
    break;

  case 83:
#line 284 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1747 "parser.c" /* yacc.c:1646  */
    break;

  case 85:
#line 289 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_BINARY_EXPRESSION, 3);}
#line 1753 "parser.c" /* yacc.c:1646  */
    break;

  case 86:
#line 290 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_UNARY_EXPRESSION, 2);}
#line 1759 "parser.c" /* yacc.c:1646  */
    break;

  case 87:
#line 291 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_UNARY_EXPRESSION, 2);}
#line 1765 "parser.c" /* yacc.c:1646  */
    break;

  case 88:
#line 291 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_BINARY_EXPRESSION, 3);}
#line 1771 "parser.c" /* yacc.c:1646  */
    break;

  case 90:
#line 297 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_BINARY_EXPRESSION, 3);}
#line 1777 "parser.c" /* yacc.c:1646  */
    break;

  case 96:
#line 308 "parser.y" /* yacc.c:1646  */
    {generateUnaryOperator(NOT);}
#line 1783 "parser.c" /* yacc.c:1646  */
    break;

  case 97:
#line 308 "parser.y" /* yacc.c:1646  */
    {generateNode(COMPONENT_UNARY_EXPRESSION, 2);}
#line 1789 "parser.c" /* yacc.c:1646  */
    break;

  case 99:
#line 314 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_VAR_EXPR); generateNode(COMPONENT_VAR, 2);}
#line 1795 "parser.c" /* yacc.c:1646  */
    break;

  case 101:
#line 318 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1801 "parser.c" /* yacc.c:1646  */
    break;

  case 102:
#line 322 "parser.y" /* yacc.c:1646  */
    {generateIdentifier(yytext);}
#line 1807 "parser.c" /* yacc.c:1646  */
    break;

  case 104:
#line 328 "parser.y" /* yacc.c:1646  */
    {generateListNode(COMPONENT_IDENTIFIER_LIST);}
#line 1813 "parser.c" /* yacc.c:1646  */
    break;

  case 105:
#line 331 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1819 "parser.c" /* yacc.c:1646  */
    break;

  case 106:
#line 332 "parser.y" /* yacc.c:1646  */
    {insertTopList();}
#line 1825 "parser.c" /* yacc.c:1646  */
    break;

  case 107:
#line 340 "parser.y" /* yacc.c:1646  */
    {generateOperator(EQUAL);}
#line 1831 "parser.c" /* yacc.c:1646  */
    break;

  case 108:
#line 341 "parser.y" /* yacc.c:1646  */
    {generateOperator(DIFFERENT);}
#line 1837 "parser.c" /* yacc.c:1646  */
    break;

  case 109:
#line 342 "parser.y" /* yacc.c:1646  */
    {generateOperator(LESS);}
#line 1843 "parser.c" /* yacc.c:1646  */
    break;

  case 110:
#line 343 "parser.y" /* yacc.c:1646  */
    {generateOperator(LESS_OR_EQUAL);}
#line 1849 "parser.c" /* yacc.c:1646  */
    break;

  case 111:
#line 344 "parser.y" /* yacc.c:1646  */
    {generateOperator(GREATER);}
#line 1855 "parser.c" /* yacc.c:1646  */
    break;

  case 112:
#line 345 "parser.y" /* yacc.c:1646  */
    {generateOperator(GREATER_OR_EQUAL);}
#line 1861 "parser.c" /* yacc.c:1646  */
    break;

  case 113:
#line 349 "parser.y" /* yacc.c:1646  */
    {generateUnaryOperator(MINUS);}
#line 1867 "parser.c" /* yacc.c:1646  */
    break;

  case 114:
#line 350 "parser.y" /* yacc.c:1646  */
    {generateUnaryOperator(PLUS);}
#line 1873 "parser.c" /* yacc.c:1646  */
    break;

  case 115:
#line 354 "parser.y" /* yacc.c:1646  */
    {generateOperator(PLUS);}
#line 1879 "parser.c" /* yacc.c:1646  */
    break;

  case 116:
#line 355 "parser.y" /* yacc.c:1646  */
    {generateOperator(MINUS);}
#line 1885 "parser.c" /* yacc.c:1646  */
    break;

  case 117:
#line 356 "parser.y" /* yacc.c:1646  */
    {generateOperator(OR);}
#line 1891 "parser.c" /* yacc.c:1646  */
    break;

  case 118:
#line 360 "parser.y" /* yacc.c:1646  */
    {generateOperator(MULTIPLY);}
#line 1897 "parser.c" /* yacc.c:1646  */
    break;

  case 119:
#line 361 "parser.y" /* yacc.c:1646  */
    {generateOperator(DIV);}
#line 1903 "parser.c" /* yacc.c:1646  */
    break;

  case 120:
#line 362 "parser.y" /* yacc.c:1646  */
    {generateOperator(AND);}
#line 1909 "parser.c" /* yacc.c:1646  */
    break;

  case 121:
#line 366 "parser.y" /* yacc.c:1646  */
    {generateInteger(yytext);}
#line 1915 "parser.c" /* yacc.c:1646  */
    break;


#line 1919 "parser.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 369 "parser.y" /* yacc.c:1906  */

