%{
/* MC900 - IMPLEMENTATION OF PROGRAMMING LANGUAGE
 * FINAL PROJECT: SL COMPILER
 * STUDENT: ELISA DELL'ARRIVA     RA: 135551
 */

/* Declarations */
#include <stdio.h>
#include "parser.h"
#include "slc.h"
#include "tree.h"

int block_components[MAX_BLOCK_COMPONENTS];
int block_i=0;

%}

%token EQUAL
%token DIFFERENT
%token LESS
%token LESS_OR_EQUAL
%token GREATER
%token GREATER_OR_EQUAL
%token PLUS
%token MINUS
%token OR
%token MULTIPLY
%token DIV
%token AND
%token UNARY
%token CLOSE_BRACE
%token CLOSE_PAREN
%token CLOSE_BRACKET
%token COLON
%token COMMA
%token ELSE
%token END_OF_FILE
%token FUNCTIONS
%token GOTO
%token IDENTIFIER
%token ASSIGN
%token IF
%token INTEGER
%token LABELS
%token NOT
%token OPEN_BRACE
%token OPEN_BRACKET
%token OPEN_PAREN
%token RETURN
%token SEMI_COLON
%token TYPES
%token VAR
%token VARS
%token VOID
%token WHILE
%token UNFINISHED_COMMENT
%token LEXICAL_ERROR

%left PLUS MINUS
%left MULT DIV

%%
/*****************
 * GRAMMAR RULES *
 *****************/

/****************
 * Initial Rule (starting symbol)
 ****************/
program
  : function END_OF_FILE {return 0;}
  ;

/******************************************************************************
 * FUNCTION:
 * can be either the main function or any declared inside the functions block
 ******************************************************************************/
function
  : VOID {generateEmpty();} function_continuation
  | identifier function_continuation
  ;
function_continuation
  : identifier formal_parameters block
    {generateNode(COMPONENT_FUNCTION, 4); block_components[block_i]=0;}
  ;

/******************************************************************************
 * BLOCKS:
 * can be either the labels, vars, types, functions blocks or the body itself
 ******************************************************************************/
block
  : labels {block_components[block_i]++;} block
  | types {block_components[block_i]++;} block
  | variables {block_components[block_i]++;} block
  | functions {block_components[block_i]++;} block
  | body {block_components[block_i]++;}
    {generateNode(COMPONENT_BLOCK, block_components[block_i]); block_components[block_i]=0;}
  ;

labels
  : LABELS identifier_list {generateNode(COMPONENT_LABELS, 1);} SEMI_COLON
  ;

types
  : TYPES type_declarations
    {generateListNode(COMPONENT_TYPES);}
  ;
type_declarations
  : identifier ASSIGN type SEMI_COLON
    {generateNode(COMPONENT_TYPE_DECLARATION, 2);}
  | type_declarations identifier ASSIGN type SEMI_COLON
    {generateNode(COMPONENT_TYPE_DECLARATION, 2); insertTopList();}
  ;
type
  : identifier
  | identifier array_type
    {generateListNode(COMPONENT_VECTOR_TYPE_LIST); generateNode(COMPONENT_VECTOR_TYPE, 2);}
  ;
array_type
  : OPEN_BRACKET CLOSE_BRACKET
    {generateCompleteNode(COMPONENT_EMPTY, 0, NULL);}
  | OPEN_BRACKET integer CLOSE_BRACKET
  | array_type OPEN_BRACKET CLOSE_BRACKET
    {generateCompleteNode(COMPONENT_EMPTY, 0, NULL); insertTopList();}
  | array_type OPEN_BRACKET integer CLOSE_BRACKET
    {insertTopList();}
  ;

variables
  : VARS vars_declaration
    {generateListNode(COMPONENT_VARS);}
  ;
vars_declaration
  : identifier_list COLON type SEMI_COLON
    {generateNode(COMPONENT_VAR_DECLARATION, 2);}
  | vars_declaration identifier_list COLON type SEMI_COLON
    {generateNode(COMPONENT_VAR_DECLARATION, 2); insertTopList();}
  ;

functions
  : FUNCTIONS {block_i++;} functions_declaration
    {generateListNode(COMPONENT_FUNCTIONS); block_i--;}
  ;
functions_declaration
  : function
  | functions_declaration function {insertTopList();}
  ;

body
  : OPEN_BRACE CLOSE_BRACE {generateEmpty();}
  | OPEN_BRACE body_statements CLOSE_BRACE {generateListNode(COMPONENT_BODY);}
  ;
body_statements
  : statement
  | body_statements statement {insertTopList();}
  ;

/***************************************************************************
 * PARAMETERS:
 * used when creating or calling a function; covers functions as parameter
 ***************************************************************************/
formal_parameters
  : OPEN_PAREN CLOSE_PAREN
    {generateEmpty();}
  | OPEN_PAREN formal_parameter CLOSE_PAREN {generateListNode(COMPONENT_PARAMETERS);}
  | OPEN_PAREN formal_parameter multiple_parameters CLOSE_PAREN
    {generateListNode(COMPONENT_PARAMETERS);}
  ;
multiple_parameters
  : SEMI_COLON formal_parameter {insertTopList();}
  | multiple_parameters SEMI_COLON formal_parameter {insertTopList();}
  ;
formal_parameter
  : expression_parameter
  | function_parameter
  ;
expression_parameter
  : VAR identifier_list COLON identifier
    {generateNode(COMPONENT_REF_PARAM, 2);}
  | identifier_list COLON identifier
    {generateNode(COMPONENT_VAL_PARAM, 2);}
  | identifier
  ;
function_parameter
  : identifier identifier formal_parameters
    {generateNode(COMPONENT_FUNCTION_AS_PARAM, 3);}
  | VOID {generateEmpty();} identifier formal_parameters
    {generateNode(COMPONENT_FUNCTION_AS_PARAM, 3);}
  ;

/***************************
 * STATEMENTS:
 * all supported statements
 ***************************/

statement
  : identifier COLON unlabeled_statement {generateNode(COMPONENT_LABEL_CALL, 2);}
  | identifier COLON compound {generateNode(COMPONENT_LABEL_CALL, 2);}
  | unlabeled_statement
  | compound
  ;

unlabeled_statement
  : assignment
  | function_call_statement
  | goto
  | return
  | conditional
  | repetitive
  | empty_statement
  ;

compound
  : OPEN_BRACE CLOSE_BRACE {generateEmpty();}
  | OPEN_BRACE compound_unlabeled_statements CLOSE_BRACE
    {generateListNode(COMPONENT_COMPOUND);}
  ;
compound_unlabeled_statements
  : unlabeled_statement
  | compound_unlabeled_statements unlabeled_statement
    {insertTopList();}
  ;

assignment
  : variable ASSIGN expression SEMI_COLON
    {generateNode(COMPONENT_ASSIGN, 2);}
  ;

function_call_statement
  : function_call SEMI_COLON
  ;
function_call
  : identifier OPEN_PAREN CLOSE_PAREN
    {generateEmpty(); generateNode(COMPONENT_FUNCALL, 2);}
  | identifier OPEN_PAREN expression_list CLOSE_PAREN
    {generateNode(COMPONENT_FUNCALL, 2);}
  ;

goto
  : GOTO identifier SEMI_COLON
    {generateNode(COMPONENT_GOTO, 1);}
  ;

return
  : RETURN SEMI_COLON
    {generateEmpty(); generateNode(COMPONENT_RETURN, 1);}
  | RETURN expression SEMI_COLON
    {generateNode(COMPONENT_RETURN, 1);}
  ;

conditional
  : IF OPEN_PAREN expression CLOSE_PAREN compound
    {generateNode(COMPONENT_IF, 2);}
  | IF OPEN_PAREN expression CLOSE_PAREN compound ELSE compound
    {generateNode(COMPONENT_IF, 3);}
  ;

repetitive
  : WHILE OPEN_PAREN expression CLOSE_PAREN compound
    {generateNode(COMPONENT_WHILE, 2);}
  ;

empty_statement
  : SEMI_COLON {generateEmpty();}
  ;

/***************************
 * EXPRESSIONS:
 * all supported expressions
 ***************************/
expression
  : simple_expression
  | simple_expression relational_operator simple_expression
    {generateNode(COMPONENT_BINARY_EXPRESSION, 3);}
  ;

expression_list
  : expression
  | expression multiple_expressions
    {generateListNode(COMPONENT_EXPRESSION_LIST);}
  ;
multiple_expressions
  : COMMA expression {insertTopList();}
  | multiple_expressions COMMA expression {insertTopList();}
  ;

simple_expression
  : term
  | term additive_operator simple_expression {generateNode(COMPONENT_BINARY_EXPRESSION, 3);}
  | unary term {generateNode(COMPONENT_UNARY_EXPRESSION, 2);}
  | unary term {generateNode(COMPONENT_UNARY_EXPRESSION, 2);} additive_operator simple_expression {generateNode(COMPONENT_BINARY_EXPRESSION, 3);}
  ;

term
  : factor
  | factor term_declaration
    {generateNode(COMPONENT_BINARY_EXPRESSION, 3);}
  ;
term_declaration
  : multiplicative_operator factor
  ;

factor
  : variable
  | integer
  | function_call
  | OPEN_PAREN expression CLOSE_PAREN
  | NOT {generateUnaryOperator(NOT);} factor {generateNode(COMPONENT_UNARY_EXPRESSION, 2);}
  ;

variable
  : identifier
  | identifier variable_expression
    {generateListNode(COMPONENT_VAR_EXPR); generateNode(COMPONENT_VAR, 2);}
  ;
variable_expression
  : OPEN_BRACKET expression CLOSE_BRACKET
  | variable_expression OPEN_BRACKET expression CLOSE_BRACKET {insertTopList();}
  ;

identifier
  : IDENTIFIER {generateIdentifier(yytext);}
  ;

identifier_list
  : identifier
  | identifier multiple_identifier
    {generateListNode(COMPONENT_IDENTIFIER_LIST);}
  ;
multiple_identifier
  : COMMA identifier {insertTopList();}
  | multiple_identifier COMMA identifier {insertTopList();}
  ;

/***************************
 * OPERATORS:
 * all supported operators
 ***************************/
relational_operator
  : EQUAL             {generateOperator(EQUAL);}
  | DIFFERENT         {generateOperator(DIFFERENT);}
  | LESS              {generateOperator(LESS);}
  | LESS_OR_EQUAL     {generateOperator(LESS_OR_EQUAL);}
  | GREATER           {generateOperator(GREATER);}
  | GREATER_OR_EQUAL  {generateOperator(GREATER_OR_EQUAL);}
  ;

unary
  : MINUS {generateUnaryOperator(MINUS);}
  | PLUS  {generateUnaryOperator(PLUS);}
  ;

additive_operator
  : PLUS  {generateOperator(PLUS);}
  | MINUS {generateOperator(MINUS);}
  | OR    {generateOperator(OR);}
  ;

multiplicative_operator
  : MULTIPLY {generateOperator(MULTIPLY);}
  | DIV      {generateOperator(DIV);}
  | AND      {generateOperator(AND);}
  ;

integer
  : INTEGER {generateInteger(yytext);}
  ;

%%
