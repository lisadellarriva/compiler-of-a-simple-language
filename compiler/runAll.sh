#!/bin/bash

echo "Rodando todos testes no parser"

for i in {0..9}
do
  ./slc < all/prog0$i.sl > all/prog0$i.my
  echo "TEST 0$i: "
  # diff all/prog0$i.my all/prog0$i.mep
done

for j in {10..34}
do
  ./slc < all/prog$j.sl > all/prog$j.my
  echo "TEST $j: "
  # diff all/myresult$j all/result$j
done

# for j in {32..33}
# do
#   ./slc < all/prog$j.sl > all/myresult$j.mep
#   echo "TEST $j: "
#   # diff all/myresult$j all/result$j
# done
