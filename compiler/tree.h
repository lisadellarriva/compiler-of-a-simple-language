/* MC900 - IMPLEMENTATION OF PROGRAMMING LANGUAGE
 * FINAL PROJECT: SL COMPILER
 * STUDENT: ELISA DELL'ARRIVA     RA: 135551
 */


/* SETTING CONSTANTS FOR LIMITS */
#define MAX_STACK_SIZE 200
#define COMPONENTS_NUMBER 8
#define MAX_BLOCK_COMPONENTS 20
extern char *yytext;
 // #define BASIC_SL
 // #ifdef BASIC_SL
 // #define BASIC_SL_ERROR printf("Syntax error");
 // #else
 // #define BASIC_SL_ERROR
 // #endif

/* Enumeration for categories of parts of the language */
typedef enum {
  COMPONENT_FUNCTIONS = 1,
  COMPONENT_FUNCTION,
  COMPONENT_FUNCALL,
  COMPONENT_PARAMETERS,
  COMPONENT_REF_PARAM,
  COMPONENT_VAL_PARAM,
  COMPONENT_FUNCTION_AS_PARAM,
  COMPONENT_LABELS,
  COMPONENT_LABEL_CALL,
  COMPONENT_VARS,
  COMPONENT_VAR_DECLARATION,
  COMPONENT_VAR_EXPR,
  COMPONENT_VAR,
  COMPONENT_TYPES,
  COMPONENT_TYPE_DECLARATION,
  COMPONENT_VECTOR_TYPE,
  COMPONENT_VECTOR_TYPE_LIST,
  COMPONENT_BLOCK,
  COMPONENT_BODY,
  COMPONENT_WHILE,
  COMPONENT_IF,
  COMPONENT_GOTO,
  COMPONENT_ASSIGN,
  COMPONENT_BINARY_EXPRESSION,
  COMPONENT_UNARY_EXPRESSION,
  COMPONENT_EXPRESSION,
  COMPONENT_EXPRESSION_LIST,
  COMPONENT_BINARY_OPERATOR,
  COMPONENT_UNARY_OPERATOR,
  COMPONENT_COMPOUND,
  COMPONENT_IDENTIFIER,
  COMPONENT_IDENTIFIER_LIST,
  COMPONENT_INTEGER,
  COMPONENT_RETURN,
  COMPONENT_EMPTY
} Category;

/* Struct representing one node of the tree */
typedef struct treeNode {
  Category        category;         //category of this node
  struct treeNode *list;            //pointer to next element of the program
  struct treeNode *components[8];   //each node can have at most 8 components
  char            *string;          //Identifier or operator
} TreeNode, *TreeNodePointer;

/* FUNCTIONS */

void *getTree();    // returns pointer to generated tree

void counts(void *p, int *functions, int *funcalls, int *whiles, int *ifs, int *bin); // counts the occurences of functions, function calls, loops, conditionals and binary expressions

/* functions to generated tree components */
void generateEmpty();
void generateIdentifier(char *value);
void generateInteger(char *value);
void generateNode(Category category, int components);
void generateCompleteNode(Category category, int components, char *literal);
void generateListNode(Category category);
void generateUnaryOperator(int op);
void generateOperator(int op);
void insertTopList();
void reverseTopList();

/* function to dump the generated tree */
void printStack();
void countList(TreeNodePointer node, Category c, int *counter);
void count(TreeNodePointer tree, Category c, int *counter);
void dumpNode(TreeNode node, int tabs);
void dumpTree(TreeNodePointer tree, int tabs);
