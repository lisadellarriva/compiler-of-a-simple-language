%{
/* MC900 - IMPLEMENTAÇÃO DE LINGUAGEM DE PROGRAMAÇÃO
 * PROJETO 1: PARSER
 * ALUNA: ELISA DELL'ARRIVA     RA: 135551
 */

/* Declarations */
#include <stdio.h>
#include "parser.h"
#include "test_parser.h"

#define BASIC_SL 1
#ifdef BASIC_SL
#define BASIC_SL_ERROR printf("Syntax error");
#else
#define BASIC_SL_ERROR
#endif

%}

%token EQUAL
%token DIFFERENT
%token LESS
%token LESS_OR_EQUAL
%token GREATER
%token GREATER_OR_EQUAL
%token PLUS
%token MINUS
%token OR
%token MULTIPLY
%token DIV
%token AND
%token UNARY
%token CLOSE_BRACE
%token CLOSE_PAREN
%token CLOSE_BRACKET
%token COLON
%token COMMA
%token ELSE
%token END_OF_FILE
%token FUNCTIONS
%token GOTO
%token IDENTIFIER
%token ASSIGN
%token IF
%token INTEGER
%token LABELS
%token NOT
%token OPEN_BRACE
%token OPEN_BRACKET
%token OPEN_PAREN
%token RETURN
%token SEMI_COLON
%token TYPES
%token VAR
%token VARS
%token VOID
%token WHILE
%token UNFINISHED_COMMENT
%token LEXICAL_ERROR

%left PLUS MINUS
%left MULT DIV
%precedence UNARY

%% /* Grammar Rules */
program
  : function END_OF_FILE {return 0;}
  ;

function
  : VOID function_continuation
  | identifier function_continuation
  ;
function_continuation
  : identifier formal_parameters block
  ;

block
  : labels block
  | types block
  | variables block
  | functions block
  | body
  ;

labels
  : LABELS identifier_list SEMI_COLON
  ;

types
  : TYPES { BASIC_SL_ERROR; } type_declarations
  ;
type_declarations
  : identifier ASSIGN type SEMI_COLON
  | type_declarations identifier ASSIGN type SEMI_COLON
  ;

variables
  : VARS vars_declaration
  ;
vars_declaration
  : identifier_list COLON type SEMI_COLON
  | vars_declaration identifier_list COLON type SEMI_COLON
  ;

functions
  : FUNCTIONS functions_declaration
  ;
functions_declaration
  : function
  | functions_declaration function
  ;

body
  : OPEN_BRACE CLOSE_BRACE
  | OPEN_BRACE body_statements CLOSE_BRACE
  ;
body_statements
  : statement
  | body_statements statement
  ;

type
  : identifier
  | identifier OPEN_BRACKET integer CLOSE_BRACKET
  ;

formal_parameters
  : OPEN_PAREN CLOSE_PAREN
  | OPEN_PAREN formal_parameter CLOSE_PAREN
  | OPEN_PAREN formal_parameter parameter_list CLOSE_PAREN
  ;
parameter_list
  : SEMI_COLON formal_parameter
  | parameter_list SEMI_COLON formal_parameter
  ;
formal_parameter
  : expression_parameter
  | function_parameter { BASIC_SL_ERROR; }
  ;
expression_parameter
  : VAR identifier_list COLON identifier
  | identifier_list COLON identifier
  | identifier
  ;
function_parameter
  : identifier identifier formal_parameters
  | VOID identifier formal_parameters
  ;

statement
  : identifier COLON unlabeled_statement
  | identifier COLON compound
  | unlabeled_statement
  | compound
  ;

variable
  : identifier
  | identifier variable_expression { BASIC_SL_ERROR; }
  ;
variable_expression
  : OPEN_BRACKET expression CLOSE_BRACKET
  | variable_expression OPEN_BRACKET expression CLOSE_BRACKET
  ;

unlabeled_statement
  : assignment
  | function_call_statement
  | goto
  | return
  | conditional
  | repetitive
  | empty_statement
  ;

assignment
  : variable ASSIGN expression SEMI_COLON
  ;

function_call_statement
  : function_call SEMI_COLON
  ;

goto
  : GOTO identifier SEMI_COLON
  ;

return
  : RETURN SEMI_COLON
  | RETURN expression SEMI_COLON
  ;

compound
  : OPEN_BRACE CLOSE_BRACE
  | OPEN_BRACE compound_unlabeled_statements CLOSE_BRACE
  ;
compound_unlabeled_statements
  : unlabeled_statement
  | compound_unlabeled_statements unlabeled_statement
  ;

conditional
  : IF OPEN_PAREN expression CLOSE_PAREN compound
  | IF OPEN_PAREN expression CLOSE_PAREN compound ELSE compound
  ;

repetitive
  : WHILE OPEN_PAREN expression CLOSE_PAREN compound
  ;

empty_statement
  : SEMI_COLON
  ;

expression
  : simple_expression
  | simple_expression relational_operator simple_expression
  ;

relational_operator
  : EQUAL
  | DIFFERENT
  | LESS
  | LESS_OR_EQUAL
  | GREATER
  | GREATER_OR_EQUAL
  ;

simple_expression
  : term
  | term sexp_multiple_terms
  | unary term
  | unary term sexp_multiple_terms
  ;
sexp_multiple_terms
  : additive_operator term
  | sexp_multiple_terms additive_operator term
  ;

unary
  : MINUS
  | PLUS
  ;

additive_operator
  : PLUS
  | MINUS
  | OR
  ;

term
  : factor
  | factor term_declaration
  ;
term_declaration
  : multiplicative_operator factor
  ;

multiplicative_operator
  : MULTIPLY
  | DIV
  | AND
  ;

factor
  : variable
  | integer
  | function_call
  | OPEN_PAREN expression CLOSE_PAREN
  | NOT factor
  ;

function_call
  : identifier OPEN_PAREN CLOSE_PAREN
  | identifier OPEN_PAREN expression_list CLOSE_PAREN
  ;

identifier_list
  : identifier
  | identifier multiple_identifier
  ;
multiple_identifier
  : COMMA identifier
  | multiple_identifier COMMA identifier
  ;

expression_list
  : expression
  | expression multiple_expressions
  ;
multiple_expressions
  : COMMA expression
  | multiple_expressions COMMA expression
  ;

empty
  :
  ;

integer
  : INTEGER
  ;

identifier
  : IDENTIFIER
  ;
%%
