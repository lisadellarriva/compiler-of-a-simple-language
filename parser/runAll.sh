#!/bin/bash

echo "Rodando todos testes no parser"

for i in {0..9}
do
  ./test_parser < all/prog0$i.sl > all/myresult0$i
  echo "TEST 0$i: "
  diff all/myresult0$i all/result0$i
done

for j in {10..33}
do
  ./test_parser < all/prog$j.sl > all/myresult$j
  echo "TEST $j: "
  diff all/myresult$j all/result$j
done
