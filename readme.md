# Compile for a simple language

This repository contains all the source files I coded as part of my final project for a course on Topics on Programming Languages, lectured by PhD Tomasz Kowaltowski, at University of Campinas.

The final project is a compiler for a simplified language, which was created based on C and Pascal by the professor.
The project is divided in three parts: the scanner, the parser and the tree build. Finally, the folder compiler contains the whole implement and working compiler.

