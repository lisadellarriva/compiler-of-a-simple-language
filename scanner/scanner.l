/* MC900 - IMPLEMENTAÇÃO DE LINGUAGEM DE PROGRAMAÇÃO
 * PROJETO 1: SCANNER
 * ALUNA: ELISA DELL'ARRIVA     RA: 135551
 */


%{
#include <stdio.h>
#include "parser.h"
#define _POSIX_C_SOURCE 1
int line_num = 1;
char *token_val;
%}

%s comment_line
%s comment

%%
<INITIAL>{
    [ \t]+          /* consume whitespaces */
    "("             return(OPEN_PAREN);
    ")"             return(CLOSE_PAREN);
    "{"             return(OPEN_BRACE);
    "}"             return(CLOSE_BRACE);
    "["             return(OPEN_BRACKET);
    "]"             return(CLOSE_BRACKET);
    "=="            return(EQUAL);
    "!="            return(DIFFERENT);
    "<"             return(LESS);
    "<="            return(LESS_OR_EQUAL);
    ">"             return(GREATER);
    ">="            return(GREATER_OR_EQUAL);
    "+"             return(PLUS);
    "-"             return(MINUS);
    "||"            return(OR);
    "*"             return(MULTIPLY);
    "/"             return(DIV);
    "&&"            return(AND);
    "!"             return(NOT);
    ":"             return(COLON);
    ","             return(COMMA);
    ";"             return(SEMI_COLON);
    "="             return(ASSIGN);
    "void"          return(VOID);
    "if"            return(IF);
    "else"          return(ELSE);
    "while"         return(WHILE);
    "goto"          return(GOTO);
    "functions"     return(FUNCTIONS);
    "var"           return(VAR);
    "vars"          return(VARS);
    "labels"        return(LABELS);
    "return"        return(RETURN);
    "types"         return(TYPES);
}

<INITIAL>"/*"           BEGIN(comment);
<comment>{
    "*/"                BEGIN(INITIAL);
    .                
    \n                  line_num++;
    <<EOF>>             return(UNFINISHED_COMMENT);    
}

<INITIAL>"//"           BEGIN(comment_line);
<comment_line>{
    .
    \n                  {line_num++; BEGIN(INITIAL);}
}


<INITIAL>{
    [0-9]+              {token_val=yytext; return(INTEGER);}
    [a-z][a-z0-9]*      {token_val=yytext; return(IDENTIFIER);}
    \n                  line_num++;
    <<EOF>>             return(END_OF_FILE);
    "$"|"#"|"?"         return(LEXICAL_ERROR);
}

%%

int yywrap() {
    return 1;
}