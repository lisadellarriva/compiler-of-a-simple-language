%{

/************************************************************************/
/*                        FILE: parser.y                                */
/************************************************************************/

/*  Last update: "parser.y: 2016-08-24 (Wed)  09:21:13 BRT (tk)"        */


/************************************************************************/
/*                                                                      */
/* Compiler for a toy language TL.                                      */
/* MO403: Implementation of Programming Languages.                      */
/*                                                                      */
/* Tomasz Kowaltowski                                                   */
/* 2nd semester, 2016                                                   */
/*                                                                      */
/************************************************************************/
/*                                                                      */
/* TL is used to illustrate application of classical UNIX tools LEX and */
/* YACC (or their equivalents FLEX and BISON). TL syntax and semantics  */
/* were inspired by C and Pascal but the language is much simpler.      */
/* It includes assignments, conditionals and iterations; the empty      */
/* statement is also included. Input and output are realized through    */
/* special variables "input" and "output" (reserved words). TL          */
/* manipulates integer values only; value 0 is used in place of "false";*/
/* non-zero values are interpreted as "true". There are no              */
/* declarations.                                                        */
/*                                                                      */
/************************************************************************/
/*                                                                      */
/* The implementation is provided in two files:                         */
/*                                                                      */
/*    -- "scanner.l", to be processed by LEX (or FLEX), producing a     */
/*        lexical analyser "scanner.c"                                  */
/*                                                                      */
/*    -- "parser.y", to be processed  by YACC (or BISON), producing a   */
/*       syntax analyser mixed with a code generator: "parser.c" and    */
/*       "parser.h"                                                     */
/*                                                                      */
/* Processing of all these files results in a compiler for TL. A typical*/
/* command sequence is:                                                 */
/*                                                                      */
/*   bison -d -v -o parser.c parser.y                                   */
/*   flex -v -t scanner.l > scanner.c                                   */
/*   gcc -DTABLE -o tlc *.c                                             */
/*                                                                      */
/* Optional command arguments "-v" produce verbose outputs in both      */
/* cases. In particular, BISON will produce a file "parser.output"      */
/* with the description of LR(1) states which can be very helpful in    */
/* debugging the grammar. If "-DTABLE" is used the compiler "tlc" will  */
/* print the symbol table. See the manuals for explanation of other     */
/* arguments. A typical compilation of a TL program is executed by:     */
/*                                                                      */
/*   tlc < prog.tl > prog.mep                                           */
/*                                                                      */
/* where 'prog.tl' is a TL source program. 'prog.mep' is the object code*/
/* for a virtual machine MEPA implemented as an interpreter.            */
/*                                                                      */
/* TL compiler is implemented as a "one-pass" program, i.e., the object */
/* code is produced in parallel with lexical and syntax analyses, and   */
/* no additional intermediate representation is used.                   */
/*                                                                      */
/* Since there are no declarations, TL compiler symbol table is very    */
/* simple. Upon first occurrence each identifier receives a displacement*/
/* (lexical level is always zero). Because of one-pass compilation,     */
/* memory allocation instruction is generated at the end, with          */
/* appropriate jumps. For the same reason, no recursive stack is used   */
/* to treat language constructs except for the labels since they are    */
/* necessary to implement nested statements like conditionals and       */
/* iterations. The LR(1) parser uses its own built-in stack.            */
/*                                                                      */
/* Instructions are generated by the set of functions "genCode1" to     */
/* "genCode6"; each function corresponds to a particular kind of MEPA   */
/* instruction and its arguments, as explained by its comment.          */
/*                                                                      */
/************************************************************************/


/* 
   
   TL GRAMMAR
   ----------
   
   Unquoted symbols [, ], {, } and | are part of the extended (BNF)
   notation (they are quoted when used as terminal symbols). 
   It is exhibited here as part of comments. Further on it
   is adapted for BISON. Last four productions ("identifier" and
   "integer" will be used by FLEX to specify the scanner).

*/

/*

program ::= 
       '{' statement_sequence '}'
       
statement_sequence ::=
       statement  { ';' statement }
       
statement ::=
       assignment  |  conditional  | iterative  | empty
       
assignment ::=
       [ variable | output ] '=' expression
       
conditional ::=
       if '(' expression ')' '{' statement_sequence  '}'
                  [ else '{' statement_sequence ] '}'
                  
iterative ::=
       while '(' expression ')' '{' statement_sequence '}'
       
empty ::=  

expression ::=
       expression '+' term | expression '-' term | term
       
term ::=
       term '*' factor | term '/' factor | factor
       
factor ::=
       variable  |  integer  | input  | '(' expression ')'
      
variable ::=
       identifier
       
identifier ::=
       letter { letter  |  digit}
       
integer ::=
       digit { digit }
       
letter ::=
       'a'|'b'|'c'|'d'|'e'|'f'|'g'|'h'|'i'|'j'|'k'|'l'|'m'|'n'|'o'|'p'|'q'|'r'|'s'|'t'|'u'|'v'|'w'|'x'|'y'|'z'
       
digit ::=
       '0'|'1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9'
  
*/   


/************************************************************************/
/*        SYNTAX ANALYSER AND CODE GENERATOR FOR MEPA                   */
/************************************************************************/


/* Declarations */
/*--------------*/

/* Token returned by  yylex for identifieres and integers           */
/* (no relation with the specifier %token used further on           */

char *token_value;       

/* Declarations avoiding warnings */
/*--------------------------------*/
/* FLEX functions */
extern int yylex (void); 
void yyerror(char *);

/* Code generation functions */
void genCode1(char *opcode);
void genCode2(char *opcode, char *ta);
void genCode3(char *opcode, int lab);  
void genCode4(int lab);
void genCode5(char *opcode, int level, int displ);
void genCode6(char *opcode, int n);

/* Label and stack functions */
void push_stack(int x);
int pop_stack();
int next_label();

/* Symbol table */
int search_insert(char *id);
    
/* Labels control for contitional and iterative statements         */

int  count_label = 0;
int  label1;
int  label2;

/* Stack for labels                                                 */

int  stack[100];
int  stack_top = -1;

/* Symbol table                                                     */

struct symbol_table_entry 
	       {
		char *ident;
                int  displ;
               };
               
struct symbol_table_entry symbol_table[100];

int displacement = -1;         /* same as index of last symbol table entry */

int aux;                       /* auxiliary variabel */

%}

%token OPEN_PAREN
%token CLOSE_PAREN
%token OPEN_BRACE
%token CLOSE_BRACE
%token ASSIGN
%token DIVIDE
%token ELSE
%token INPUT
%token IDENTIFIER
%token IF
%token PLUS
%token MINUS
%token MULTIPLY
%token INTEGER
%token OUTPUT
%token SEMI_COLUMN
%token WHILE

%%

program
	: 
				{genCode1("MAIN"); label1 = next_label();  
                                 label2 = next_label(); push_stack(label1); 
                                 push_stack(label2); genCode3("JUMP",label1);
                                 genCode4(label2);} 
          braced_statement_sequence 
          			{genCode6("DLOC",displacement+1); genCode1("STOP");  
                                 label2 = pop_stack(); label1 = pop_stack();
                                 genCode4(label1); genCode6("ALOC", displacement+1);
                                 genCode3("JUMP",label2); genCode1("END");}
        ;
        
braced_statement_sequence 
        : OPEN_BRACE statement_sequence CLOSE_BRACE
         
statement_sequence
	: statement_sequence SEMI_COLUMN statement
        | statement
        ;
        
statement
	: assignment
        | conditional
        | iterative
        | empty
        ;
        
assignment
	: variable 
        			{aux=search_insert(token_value);}
          ASSIGN expression
        			{genCode5("STVL",0,symbol_table[aux].displ);}
        | OUTPUT ASSIGN expression
                                {genCode1("PRNT");}
        ;
        
conditional
	: IF OPEN_PAREN expression CLOSE_PAREN
        			{label1 = next_label(); genCode2("LDCT","0"); genCode1("DIFF");
				 genCode3("JMPF",label1); push_stack(label1);}
          braced_statement_sequence conditional_alternative
          			{label1 = pop_stack(); genCode4(label1);}
        ;
        
iterative
        : WHILE
        			{label1 = next_label(); label2 = next_label(); genCode4(label1);
                                 push_stack(label1); push_stack(label2);}
          OPEN_PAREN expression CLOSE_PAREN
          			{genCode2("LDCT","0"); genCode1("DIFF"); genCode3("JMPF",label2);}
          braced_statement_sequence
          			{label2=pop_stack(); label1=pop_stack();
				 genCode3("JUMP",label1); genCode4(label2);}
        ;
        
conditional_alternative
	: ELSE
          			{label1 = pop_stack(); label2 = next_label(); 
                                 push_stack(label2); genCode3("JUMP",label2); genCode4(label1); }        			
          braced_statement_sequence
        | empty
        ;

empty
	: /* empty! */
        ;
        
expression
	: expression PLUS term
        			{genCode1("ADDD");}
        | expression MINUS term
        			{genCode1("SUBT");}
        | term
        ;
        
term
	: term MULTIPLY factor
        			{genCode1("MULT");}
        | term DIVIDE factor
        			{genCode1("DIVI");}
        | factor
        ;
        
factor
	: variable
        			{genCode5("LDVL",0,symbol_table[search_insert(token_value)].displ);}
        | INTEGER
        			{genCode2("LDCT",token_value);}
        | INPUT
                                {genCode1("READ");}
        | OPEN_PAREN expression CLOSE_PAREN
        ;

variable
	: IDENTIFIER
        ;

%%        
        
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void yyerror(char *s)
/* Will be called by the generated parser. */
         {
	   fprintf(stderr,"Bison message: %s\n",s);
	   fprintf(stderr,"Last token returned: %s\n",token_value);
           exit(0);
         }

void genCode1(char *opcode)
/* Instruction of the form:  OPCODE  (no args) */
         {
          printf("      %s\n",opcode);
         }
         
void genCode2(char *opcode, char *ta)
/* Instruction of the form:  OPCODE STRING */
         {
          printf("      %s  %s\n",opcode,ta);
         }
         
void genCode3(char *opcode, int lab)
/* Instruction of the form:  OPCODE INT (label arg) */
         {
          printf("      %s  L%d\n",opcode,lab);
         }
         
void genCode4(int lab)
/* Instruction of the form:  LABEL OPCODE  (no args) */
         {
          printf("L%d:   NOOP\n",lab);
         }
         
void genCode5(char *opcode, int level, int displ)
/* Instruction of the form:  OPCODE INT INT (variable access) */
         {
          printf("      %s  %d,%d\n",opcode,level,displ);
         }
         
void genCode6(char *opcode, int n)
/* Instruction of the form:  OPCODE INT */
         {
          printf("      %s  %d\n",opcode,n);
         }
         
int next_label()
/* Provides the next label number. */
	{
         return(count_label+=1);
        }
       
void push_stack(int x)
/* Push a label number */
        {
         stack[++stack_top] = x;
        }

int pop_stack()
/* Pop a label number */
	{
         int aux;
         aux = stack[stack_top--];
         return(aux);
        }
         
int search_insert(char *id)
/* Search id in the symbol table; insert if not present; return its
   displacement */
        {
         int i = 0; int res = -1;

         while(i<=displacement && 
               (res=strcmp(id,symbol_table[i].ident))!=0)
            {
	      i++;
            };
         
         if (res==0)
            return(i);
           else
            {
	    displacement++;
            strcpy((symbol_table[displacement].ident=malloc(strlen(id))),id);
            symbol_table[displacement].displ = displacement;
            return(displacement);
            }
         }


/************************/
/* Parser main function */
/************************/

int main() {

  int aux = yyparse();  /* call parser */ 
  
/* Print symbol table if compiled with -DTABLE */
#ifdef TABLE
       int i;
       fprintf(stderr,"\n\nSymbol table:\n");
       fprintf(stderr,"-------------\n");
       for(i=0;i<=displacement;i++)
	 {
	  fprintf(stderr,"%10s:%5d\n",symbol_table[i].ident,symbol_table[i].displ);
	 }
       fprintf(stderr,"\n");
#endif

        return(aux);
        
}
      
