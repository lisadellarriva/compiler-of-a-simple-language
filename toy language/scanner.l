%{

/************************************************************************/
/*                       FILE: scanner.l                                */
/************************************************************************/

/*  Last update: "scanner.l: 2016-07-31 (Sun)  11:07:20 BRT (tk)"       */

/************************************************************************/
/*  See file "parser.y" for the description of the language TL          */
/************************************************************************/


/* Inclusion of the file generated by bison or yacc */

# include "parser.h"

/* Variable "token_value" is declared in "parser.y" */

extern char *token_value;  

%}

%%

%{ 
/* TOKEN SPECIFICATION */
%}



%{
/* Blanks etc */
%}

[ \t\n]                 ;

%{
/* Reserved words */
%}

else                    return(ELSE);
if                      return(IF);
input                   return(INPUT);
output                  return(OUTPUT);
while                   return(WHILE);

%{
/* Symbols */
%}

"("			return(OPEN_PAREN);
")"			return(CLOSE_PAREN);
"{"                     return(OPEN_BRACE);
"}"                     return(CLOSE_BRACE);
"="			return(ASSIGN);
"+"			return(PLUS);
"-"			return(MINUS);
"*"			return(MULTIPLY);
"/"                     return(DIVIDE);
";"			return(SEMI_COLUMN);

%{
/* Identifiers and integers */
%}

[a-z][a-z0-9]*		{strcpy((token_value=malloc(yyleng+1)),yytext);
                         return(IDENTIFIER);}
[0-9]+			{strcpy((token_value=malloc(yyleng+1)),yytext);
                         return(INTEGER);}


%%

int yywrap(){
        return 1;
        }



