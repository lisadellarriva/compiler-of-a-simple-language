

/*
   Test of the tree builder SL.
   
   Returns code execution 0 even in case of errors so as not to
   confuse SuSy.
*/

/* Last update: "test_tree.c: 2016-09-16 (Fri)  14:27:05 BRT (tk)" */

#define MAIN

#include <stdio.h>
#include <stdlib.h>
#include "test_tree.h"
#include "tree.h"

extern int line_num;

int yyerror(char *s) {
  
  printf("Error detected on line %d.\n",line_num);
  printf("Last token read: '%s'\n\n",yytext);
  return 0;
  
}

int main(int argc, char **argv) {

  int functions, funcalls, whiles, ifs, bin;
  void *tree; // test program does not need to know the type
  
  if (yyparse()!=0) 
    return 0;  // error message printed already

  tree = getTree();
  counts(tree, &functions, &funcalls, &whiles, &ifs, &bin);
  printf("%d function declarations\n", functions);
  printf("%d function calls\n", funcalls);
  printf("%d loops\n", whiles);
  printf("%d conditionals\n", ifs);
  printf("%d binary operations\n", bin);
  
  return 0;

}

