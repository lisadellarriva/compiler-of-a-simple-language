/* MC900 - IMPLEMENTATION OF PROGRAMMING LANGUAGE
 * PROJECT PART 3: TREE BUILDER
 * STUDENT: ELISA DELL'ARRIVA     RA: 135551
 */

#include <stdio.h>
#include <stdlib.h>
#include "tree.h"
#include "test_tree.h"

/* Stack */
TreeNodePointer stack[MAX_STACK_SIZE];
int top = 0;

/* Returns the root of the tree */
void *getTree() {
  // printf("getTree\n");
  return (void *) stack[--top];
}

/* Insert node at top of the stack  */
void insertTopList() {
  // printf("insertTopList\n");
  TreeNodePointer t = stack[--top];
  TreeNodePointer s = stack[top-1];

  t->list = s;
  stack[top-1] = t;
}

/* Reverse the list that is at the top of the stack */
void reverseTopList() {
  // printf("reverseTopList\n");
  TreeNodePointer l, aux, rl=NULL;

  l = stack[top-1];
  while (l != NULL) {
    aux = l->list;
    l->list = rl;
    rl = l;
    l = aux;
  }
  stack[top-1] = rl;
}

/* Generate Empty Statement */
void generateEmpty() {
  // printf("generateEmpty\n");
  // generateCompleteNode(COMPONENT_EMPTY, 0, NULL);
  stack[top] = NULL;
  top++;
}

/* Generate component Identifier */
void generateIdentifier(char *value) {
  // printf("generateIdentifier\n");
  // printf("\t");
  generateCompleteNode(COMPONENT_IDENTIFIER, 0, value);
}

/* Generate component Integer */
void generateInteger(char *value) {
  // printf("generateInteger\n");
  // printf("\t");
  generateCompleteNode(COMPONENT_INTEGER, 0, value);
}

/* Generate Node with 2 arguments
 * @params  category: node Category
            components: number of components of new node
 */
void generateNode(Category category, int components) {
  // printf("generateNode: %d\n", category);
  // printf("\t");
  generateCompleteNode(category, components, NULL);
}

/* Generate Node with 3 arguments
 * @params  category:   node Category
            components: number of components of new node
            literal:    string value of new node
 */
void generateCompleteNode(Category category, int components, char *literal) {
  // printf("generateCompleteNode: %d - %d\n", category, components);
  int i, j;
  TreeNodePointer newNode = malloc(sizeof(TreeNode));

  //fullfill new node information
  newNode->category = category;
  newNode->list = NULL;
  newNode->string = literal;
  for (i=0; i<components; i++) {
    newNode->components[i] = stack[top - components + i];
  }
  for (j=components; j<COMPONENTS_NUMBER; j++) {
    newNode->components[j] = NULL;
  }

  top = top - (components - 1);           //adjust top of the stack
  stack[top-1] = newNode;                 //put new node at the top of the stack
}

/* Generate node which is a list of components */
void generateListNode(Category category) {
  // printf("generateListNode\n");
  // printf("\t");
  reverseTopList();
  // printf("\t");
  generateCompleteNode(category, 0, NULL);
  // printf("\t");
  insertTopList();
}

/* Generate Unary Operator symbol */
void generateUnaryOperator(int op) {
  // printf("generateUnaryOperator\n");
  // printf("\t");
  switch (op) {
    case 264: //PLUS
      generateCompleteNode(COMPONENT_UNARY_OPERATOR, 0, "+");
      break;
   case 265: //MINUS:
     generateCompleteNode(COMPONENT_UNARY_OPERATOR, 0, "-");
     break;
   case 285: //NOT:
    generateCompleteNode(COMPONENT_UNARY_OPERATOR, 0, "!");
    break;
  }
}

/* Generate Bianary Operator symbol */
void generateOperator(int op) {
  // printf("generateOperator\n");
  // printf("\t");
  switch (op) {
    case 264: //PLUS
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, "+");
      break;
    case 265: //MINUS:
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, "-");
      break;
    case 267: //MULTIPLY:
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, "*");
      break;
    case 268: //DIV:
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, "/");
      break;
    case 269: //AND:
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, "&&");
      break;
    case 266: //OR:
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, "||");
      break;
    case 258: //EQUAL:
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, "==");
      break;
    case 260: //LESS:
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, "<");
      break;
    case 261: //LESS_OR_EQUAL:
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, "<=");
      break;
    case 262: //GREATER:
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, ">");
      break;
    case 263: //GREATER_OR_EQUAL:
      generateCompleteNode(COMPONENT_BINARY_OPERATOR, 0, ">=");
      break;
  }
}

/* Auxiliar function for counting occurences */
void countList(TreeNodePointer node, Category c, int *counter) {
  if (node != NULL) {
    if (node->category == c) {
      (*counter)++;
    }
    for (int i=0; i<COMPONENTS_NUMBER; i++) {
      if (node->components[i] != NULL) {
        count(node->components[i], c, counter);
      }
    }
  }
}

/* Count the occurences of a given category */
void count(TreeNodePointer tree, Category c, int *counter) {
  TreeNodePointer aux;
  if (tree != NULL) {
    if (tree->category == c) {
      (*counter)++;
    }

    // count the list item
    for (aux = tree->list; aux != NULL; aux = aux->list) {
      countList(aux, c, counter);
    }

    // count the components
    for (int i=0; i<COMPONENTS_NUMBER; i++) {
      if (tree->components[i] != NULL) {
        aux = tree->components[i];
        count(aux, c, counter);
      }
    }
  }
}

/* Counts the occurences of each category */
void counts(void *p, int *functions, int *funcalls, int *whiles, int *ifs, int *bin) {
  // printf("counts\n");
  int c=0;
  TreeNodePointer aux = p;

  // dumpTree(aux, 0);

  count(aux, COMPONENT_FUNCTION, &c);
  *functions = c;
  c = 0; aux = p;
  count(aux, COMPONENT_FUNCALL, &c);
  *funcalls = c;
  c = 0; aux = p;
  count(aux, COMPONENT_WHILE, &c);
  *whiles = c;
  c = 0; aux = p;
  count(aux, COMPONENT_IF, &c);
  *ifs = c;
  c = 0; aux = p;
  count(aux, COMPONENT_BINARY_OPERATOR, &c);
  *bin = c;
}
